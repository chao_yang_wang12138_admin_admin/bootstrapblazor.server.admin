using GMS.NET.Web.Components.Data;
using GMS.NET.Web.Components.Shared.Global.Misc;
using GMS.NET.Web.Components.Shared.Global.Misc.Logger;
using System.Text;

var builder = WebApplication.CreateBuilder(args);
var serverHost = builder.Configuration.GetValue<string>(AppSetting.ServerHost);

// Add Services to the Container. Avoid Chinese garbled code
Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

// Add Services to the Container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();

// Add Bootstrap Blazor Services
builder.Services.AddBootstrapBlazor();

// Add the Table data service operation class
builder.Services.AddTableDemoDataService();

// 增加 Table Excel 导出服务
builder.Services.AddBootstrapBlazorTableExcelExport();

//ILogger日志
builder.Services.AddSingleton<ILoggerProvider>(new LoggerProvider($"Logger/{DateTime.Now:yyyyMMdd}.txt"));
builder.Services.AddLogging();

// Add GmsBootstrapAdmin Services
builder.Services.AddGMSNETWebService(serverHost);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

// Customer Service Information
app.UseBootstrapBlazor();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.Run();
