﻿namespace GMS.NET.Dto.System.Layout.Ouput
{
    public class MenuItemSync
    {
        public string? Text { get; set; }
        public string? Icon { get; set; }
        public string? Url { get; set; }
        public string? CssClass { get;set; }
        public List<MenuItemSync>? Items { get; set; }
    }
}
