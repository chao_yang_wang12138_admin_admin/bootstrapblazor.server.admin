﻿using GMS.NET.Dto.System.Custs.Enums;

namespace GMS.NET.Dto.System.Custs.Shared
{
    public class CustsData
    {
        /// <summary>
        /// Desc:客户Guid
        /// </summary>           
        public Guid Id { get; set; }

        /// <summary>
        /// Desc:客户简称
        /// </summary>
        [Required(ErrorMessage = "客户简称不可为空")]
        [Display(Name = "客户简称")]
        public string? Cust_Name { get; set; }

        /// <summary>
        /// Desc:客户全称
        /// </summary>   
        [Required(ErrorMessage = "客户全称不可为空")]
        [Display(Name = "客户全称")]
        public string? Cust_FullName { get; set; }

        ///// <summary>
        ///// Desc:客户数据库
        ///// </summary>
        //[Display(Name = "客户数据库")]
        //public string? Cust_Connection { get; set; }

        /// <summary>
        /// Desc:客户电话
        /// </summary>  
        [Display(Name = "客户电话")]
        [Phone(ErrorMessage ="客户电话格式错误")]
        [Required(ErrorMessage ="客户电话不可为空")]
        public string? Cust_Phone { get; set; }

        /// <summary>
        /// Desc:客户类型1默认类型2VIP
        /// </summary>
        [Required]
        [Display(Name = "客户类型")]
        public Cust_Type Cust_Type { get; set; }

        /// <summary>
        /// Desc:客户地址
        /// </summary>  
        [Display(Name = "客户地址")]
        public string? Cust_Address { get; set; }

        /// <summary>
        /// Desc:客户状态1正常0禁用2删除
        /// </summary>           
        [Required]
        [Display(Name = "客户状态")]
        public Cust_Status Cust_Status { get; set; }

        /// <summary>
        /// Desc:删除时间
        /// </summary> 
        [Display(Name = "销毁日期")]
        public DateTime? Cust_Deltime { get; set; }

        /// <summary>
        /// Desc:客户邮箱
        /// </summary> 
        [Display(Name = "客户邮箱")]
        [EmailAddress(ErrorMessage ="客户邮箱格式错误")]
        [Required(ErrorMessage = "客户邮箱不可为空")]
        public string? Cust_Email { get; set; }

        /// <summary>
        /// Desc:客户备注
        /// </summary>
        [Display(Name = "客户备注")]
        public string? Cust_Remark { get; set; }
    }
}
