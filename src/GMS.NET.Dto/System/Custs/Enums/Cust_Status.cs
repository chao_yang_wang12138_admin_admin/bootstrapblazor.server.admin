﻿
namespace GMS.NET.Dto.System.Custs.Enums
{
    public enum Cust_Status
    {
        [Display(Name = "正常")]
        normal = 1,
        [Display(Name = "禁用")]
        disabled = 0,
        [Display(Name = "删除")]
        delete = 2
    }
}
