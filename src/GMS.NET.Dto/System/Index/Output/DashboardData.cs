﻿namespace GMS.NET.Dto.System.Index.Output
{
    public class DashboardData
    {
        public int TestAllCount { get; set; }
        public int TestApprovedAllCount { get; set; }
        public double TestApprovedAllScale { get; set; }
        public int TestYearCount { get; set; }
        public int TestApprovedYearCount { get; set; }
        public double TestApprovedYearScale { get; set; }
        public int TestMonthCount { get; set; }
        public int TestApprovedMonthCount { get; set; }
        public double TestApprovedMonthScale { get; set; }
        public int TestDayCount { get; set; }
        public int TestApprovedDayCount { get; set; }
        public double TestApprovedDayScale { get; set; }
        public List<TestKKSGroupData>? TestKKSGroupList { get; set; }
        public List<TestDayGroupData>? TestDayGroupList { get; set; }
    }
}
