﻿namespace GMS.NET.Dto.System.Index.Output
{
    public class TestDayGroupData
    {
        public int Count { get; set; }
        public int Key { get; set; }
    }
}
