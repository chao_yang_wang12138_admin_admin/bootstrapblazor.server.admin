﻿namespace GMS.NET.Dto.System.Index.Output
{
    public class TestKKSGroupData
    {
        public int Count { get; set; }
        public string? KKS { get; set; }
        public string? NAM { get; set; }
        public double Percent { get; set; }
        public int ApprovedCount { get; set; }
    }
}
