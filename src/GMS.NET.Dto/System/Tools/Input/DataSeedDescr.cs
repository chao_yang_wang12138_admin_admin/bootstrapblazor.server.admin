﻿
namespace GMS.NET.Dto.System.Tools.Input
{
    public class DataSeedDescr
    {
        public string? EntityNameSpace { get; set; }
        public string? DataSeedEntityNameSpace { get;set; }
    }
}
