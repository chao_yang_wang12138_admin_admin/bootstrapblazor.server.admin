﻿using GMS.NET.Dto.System.AccRoles.Enums;

namespace GMS.NET.Dto.System.AccRoles.Shared
{
    public class RolesData
    {
        /// <summary>
        /// Desc:角色Guid
        /// </summary>           
        public Guid Id { get; set; }

        /// <summary>
        /// Desc:角色名称
        /// </summary> 
        [Display(Name = "角色名称")]
        [Required(ErrorMessage = "角色名称不可为空")]
        public string? Role_Name { get; set; }

        /// <summary>
        /// Desc:角色所属客户Id
        /// </summary>  
        public Guid Role_Custs { get; set; }

        /// <summary>
        /// Desc:角色所属客户名称
        /// </summary>  
        [Display(Name = "客户名称")]
        public string? Role_CustsName { get; set; }

        /// <summary>
        /// Desc:角色状态1正常2禁用
        /// </summary>   
        [Display(Name = "角色状态")]
        [Required(ErrorMessage = "角色状态不可为空")]
        public Role_Status Role_Status { get; set; }

        /// <summary>
        /// Desc:角色备注
        /// </summary>           
        [Display(Name = "角色备注")]
        public string? Role_Remark { get; set; }
    }
}
