﻿namespace GMS.NET.Dto.System.AccRoles.Input
{
    /// <summary>
    /// 角色-资源授权信息
    /// </summary>
    public class AccRess
    {
        public List<Guid>? Ress { get; set; }
        public Guid Role { get; set; }
    }
}
