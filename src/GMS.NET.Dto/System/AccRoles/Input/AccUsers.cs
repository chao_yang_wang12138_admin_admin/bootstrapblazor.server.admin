﻿namespace GMS.NET.Dto.System.AccRoles.Input
{
    /// <summary>
    /// 用户-角色授权信息
    /// </summary>
    public class AccUsers
    {
        public List<Guid>? Users { get; set; }
        public Guid Role { get; set; }
    }
}
