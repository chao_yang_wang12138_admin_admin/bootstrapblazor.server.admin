﻿
namespace GMS.NET.Dto.System.AccRoles.Output
{
    /// <summary>
    /// 待选用户
    /// </summary>
    public class WaitUsers
    {
        /// <summary>
        /// Desc:主键Guid
        /// </summary>   
        public Guid Id { get; set; }

        /// <summary>
        /// Desc:用户账号
        /// </summary>
        [Display(Name = "用户账号")]
        public string? User_Account { get; set; }

        /// <summary>
        /// Desc:用户名称
        /// </summary>
        [Display(Name = "用户名称")]
        public string? User_Name { get; set; }

        /// <summary>
        /// Desc:客户名称
        /// </summary>
        [Display(Name = "客户名称")]
        public string? Custs_Name { get; set; }

        /// <summary>
        /// Desc:角色名称
        /// </summary>
        [Display(Name = "角色名称")]
        public string? Role_Name { get; set; }
    }
}
