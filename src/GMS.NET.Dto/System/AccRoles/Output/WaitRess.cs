﻿namespace GMS.NET.Dto.System.AccRoles.Output
{
    /// <summary>
    /// 待选资源
    /// </summary>
    public class WaitRess
    {
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public string? Text { get; set; }
        public string? Icon { get; set; }
        public bool IsDisabled { get; set; } = false;
        public bool HasChildren { get; set; }
        public bool IsChecked { get;set; }
        public List<WaitRess>? Children { get; set; } = new();
    }
}
