﻿
namespace GMS.NET.Dto.System.AccRoles.Enums
{
    public enum Role_Status
    {
        [Display(Name = "正常")]
        normal=1,
        [Display(Name = "禁用")]
        disabled=2
    }
}
