﻿namespace GMS.NET.Dto.System.Auth.Output
{
    public class LoginToken
    {
        /// <summary>
        /// 用户令牌
        /// </summary>
        public  string? AccessToken{ get; set; }
        /// <summary>
        /// 生命周期
        /// </summary>
        public  int TimeLcs { get; set; }
}
}
