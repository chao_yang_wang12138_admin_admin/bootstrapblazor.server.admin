﻿
namespace GMS.NET.Dto.System.Auth.Input
{
    public class SendEmail
    {
        public string? Email { get; set; }
        public string? EmailCode { get; set; }
    }
}
