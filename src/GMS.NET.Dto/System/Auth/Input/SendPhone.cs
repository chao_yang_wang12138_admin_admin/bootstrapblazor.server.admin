﻿namespace GMS.NET.Dto.System.Auth.Input
{
    public class SendPhone
    {
        public string? Phone { get; set; }
        public string? PhoneCode { get; set; }
    }
}
