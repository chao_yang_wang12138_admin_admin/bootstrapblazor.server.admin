﻿namespace GMS.NET.Dto.System.Auth.Shared
{
    public class LoginAuth
    {
        /// <summary>
        /// 密文
        /// </summary>
        public string? Ciphertext { get; set; }
    }
}
