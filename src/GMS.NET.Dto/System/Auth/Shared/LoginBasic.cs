﻿using System.ComponentModel.DataAnnotations;

namespace GMS.NET.Dto.System.Auth.Shared
{
    public class LoginBasic
    {
        /// <summary>
        /// 账号
        /// </summary>
        public string? Account { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string? Password { get; set; }
        /// <summary>
        /// 登陆模式
        /// </summary>
        public LoginType Mode { get; set; } = LoginType.Default;
    }

    public enum LoginType
    {
        Default,
        Email,
        Phone
    }
}
