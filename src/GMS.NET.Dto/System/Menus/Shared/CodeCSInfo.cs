﻿
namespace GMS.NET.Dto.System.Menus.Shared
{
    public class CodeCSInfo
    {
        public string? Res_CsFileName { get; set; }
        public string? Res_CsFileCatalog { get; set; }
    }
}
