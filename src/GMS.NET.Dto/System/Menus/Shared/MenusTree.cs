﻿using GMS.NET.Dto.System.Menus.Enums;

namespace GMS.NET.Dto.System.Menus.Shared
{
    public class MenusTree
    {
        /// <summary>
        /// Desc:资源Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Desc:资源类型
        /// </summary> 
        [Required(ErrorMessage = "请选择资源类型")]
        [Display(Name = "资源类型")]
        public Res_Type Res_Type { get; set; }

        /// <summary>
        /// Desc:资源名称
        /// </summary>
        [Required(ErrorMessage = "请填写资源名称")]
        [Display(Name = "资源名称")]
        public string? Res_Name { get; set; }

        /// <summary>
        /// Desc:资源图标
        /// </summary>
        [Display(Name = "资源图标")]
        public string? Res_Icon { get; set; }

        /// <summary>
        /// Desc:资源样式
        /// </summary>  
        [Display(Name = "资源样式")]
        public string? Res_CssClass { get; set; }

        /// <summary>
        /// Desc:资源状态
        /// </summary>
        [Required(ErrorMessage = "请选择资源状态")]
        [Display(Name = "资源状态")]
        public Res_Disabled Res_Disabled { get; set; }

        /// <summary>
        /// Desc:资源地址
        /// </summary> 
        [Display(Name = "资源地址")]
        public string? Res_Url { get; set; }

        /// <summary>
        /// Desc:父级资源
        /// </summary>           
        public Guid Res_ParentId { get; set; }

        /// <summary>
        /// Desc:资源排序
        /// </summary>     
        [Display(Name = "资源排序")]
        public int Res_Sort { get; set; }

        /// <summary>
        /// Desc:资源备注
        /// </summary> 
        [Display(Name = "资源备注")]
        public string? Res_Remark { get; set; }

        /// <summary>
        /// Desc:代码名称(接口名称/类名)选填
        /// </summary>           
        [Display(Name = "代码名称")]
        public string? Res_CodeName { get; set; }

        /// <summary>
        /// Desc:生成api方法需要指定的类名称项((选填)
        /// </summary>           
        [Display(Name = "所属类名")]
        public string? Res_CsFileName { get; set; }

        /// <summary>
        /// Desc:生成方法类需要指定的目录名称默认在服务器层Service文件夹下(选填)
        /// </summary>           
        [Display(Name = "文件目录")]
        public string? Res_CsFileCatalog { get; set; }

        /// <summary>
        /// 是否开启代码生成
        /// </summary>
        [Display(Name = "代码生成")]
        public bool IsOpenCodeCreate { get; set; } = false;
    }
}
