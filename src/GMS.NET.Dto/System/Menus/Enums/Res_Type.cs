﻿using System.ComponentModel.DataAnnotations;

namespace GMS.NET.Dto.System.Menus.Enums
{
    public enum Res_Type
    {
        [Display(Name = "系统目录")]
        sysdirectory,
        [Display(Name = "系统菜单")]
        sysmenu,
        [Display(Name = "api接口")]
        apiInterface,
        [Display(Name = "字段")]
        field
    }
}
