﻿using System.ComponentModel.DataAnnotations;

namespace GMS.NET.Dto.System.Menus.Enums
{
    public enum Res_Disabled
    {
        [Display(Name = "正常")]
        normal,
        [Display(Name = "禁用")]
        disabled
    }
}
