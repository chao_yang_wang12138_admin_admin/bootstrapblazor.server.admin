﻿
namespace GMS.NET.Dto.System.Test.Output
{
    public class TestData
    {
        /// <summary>
		/// Desc:Guid
		/// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Desc:测试字段
        /// </summary>
        [Required(ErrorMessage = "测试字段不可为空")]
        [Display(Name = "测试字段")]
        public string? Test { get; set; }

        /// <summary>
        /// Desc:测试字段2
        /// </summary>
        [Required(ErrorMessage = "测试字段2不可为空")]
        [Display(Name = "测试字段2")]
        public DateTime? Test2 { get; set; }

        /// <summary>
        /// Desc:创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime Creationtime { get; set; }

        /// <summary>
        /// Desc:创建人
        /// </summary>
        [Display(Name = "创建人")]
        public string? Creator { get; set; }

        /// <summary>
        /// Desc:修改时间
        /// </summary>
        [Display(Name = "修改时间")]
        public DateTime Modifytime { get; set; }

        /// <summary>
        /// Desc:修改人
        /// </summary>
        [Display(Name = "修改人")]
        public string? Modifier { get; set; }
    }
}
