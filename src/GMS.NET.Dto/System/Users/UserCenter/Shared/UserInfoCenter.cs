﻿
namespace GMS.NET.Dto.System.Users.UserCenter.Shared
{
    public class UserInfoCenter
    {
        [Display(Name = "您的账号")]
        public string? User_Account { get; set; }
        [Display(Name = "您的用户名")]
        public string? User_Name { get; set; }
        [Phone(ErrorMessage = "错误的手机格式")]
        [Display(Name = "您的手机号")]
        public string? User_Phone { get; set; }
        public string? User_Phone_Code { get; set; }
        [EmailAddress(ErrorMessage ="错误的邮箱格式")]
        [Display(Name = "您的邮箱")]
        public string? User_Email { get; set; }
        public string? User_Email_Code { get; set; }

        [Display(Name = "您的Ip")]
        public string? User_Ip { get; set; }
        [Display(Name = "您的地址")]
        public string? User_Address { get; set; }
    }
}
