﻿namespace GMS.NET.Dto.System.Users.UserCenter.Input
{
    public class UserInfoPas
    {
        [Required(ErrorMessage = "原密码不可为空")]
        [Display(Name ="原密码")]
        public string? OldPassWord { get; set; }
        [Required(ErrorMessage = "新密码不可为空")]
        [Display(Name = "新密码")]
        public string? NewPassWord { get; set; }
        [Required(ErrorMessage = "新密码不可为空")]
        [Display(Name = "新密码")]
        public string? NewPassWordTo { get; set; }
    }
}
