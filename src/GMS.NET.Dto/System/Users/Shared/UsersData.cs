﻿using GMS.NET.Dto.System.Users.Enums;

namespace GMS.NET.Dto.System.Users.Shared
{
    public class UsersData
    {
        /// <summary>
        /// Desc:主键Guid
        /// </summary>   
        public Guid Id { get; set; }

        /// <summary>
        /// Desc:用户账号
        /// </summary>
        [Required(ErrorMessage ="请输入用户账号")]
        [Display(Name ="用户账号")]
        public string? User_Account { get; set; }

        /// <summary>
        /// Desc:用户密码
        /// </summary>
        //public string? User_PassWord { get; set; }

        /// <summary>
        /// Desc:用户名称
        /// </summary>
        [Required(ErrorMessage = "请输入用户名称")]
        [Display(Name = "用户名称")]
        public string? User_Name { get; set; }

        /// <summary>
        /// Desc:所属组织
        /// </summary>     
        [Display(Name = "所属客户")]
        public string? User_CusName { get; set; }

        /// <summary>
        /// Desc:所属组织
        /// </summary>           
        public Guid User_CusId { get; set; }

        /// <summary>
        /// Desc:手机号码
        /// </summary>    
        [Phone(ErrorMessage = "手机号码格式不正确")]
        [Display(Name = "手机号码")]
        public string? User_Phone { get; set; }

        /// <summary>
        /// Desc:邮箱地址
        /// </summary> 
        [EmailAddress(ErrorMessage = "邮箱地址格式不正确")]
        [Display(Name = "邮箱地址")]
        public string? User_Email { get; set; }

        /// <summary>
        /// Desc:用户角色
        /// </summary>       
        [Display(Name = "用户角色")]
        public string? User_RoleName { get; set; }

        /// <summary>
        /// Desc:用户角色Id
        /// </summary>           
        public Guid User_RoleId { get; set; }

        /// <summary>
        /// Desc:用户状态1正常0禁用
        /// </summary>
        [Display(Name = "用户状态")]
        public User_Status User_Status { get; set; }

        /// <summary>
        /// Desc:用户类型1系统用户2API接口
        /// </summary>   
        [Display(Name = "用户类型")]
        public User_Type User_Type { get; set; }

        /// <summary>
        /// Desc:用户备注
        /// </summary>  
        [Display(Name = "用户备注")]
        public string? User_Remark { get; set; }

        /// <summary>
        /// Desc:其他内容
        /// </summary>  
        public string? CipherText { get; set; }
    }
}
