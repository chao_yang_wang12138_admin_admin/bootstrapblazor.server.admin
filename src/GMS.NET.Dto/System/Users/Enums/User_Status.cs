﻿namespace GMS.NET.Dto.System.Users.Enums
{
    public enum User_Status
    {
        [Display(Name = "正常")]
        normal=1,
        [Display(Name = "禁用")]
        disabled=0
    }
}
