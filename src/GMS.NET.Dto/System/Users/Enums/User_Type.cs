﻿using System.ComponentModel.DataAnnotations;

namespace GMS.NET.Dto.System.Users.Enums
{
    public enum User_Type
    {
        [Display(Name = "系统用户")]
        systemuser=1,
        [Display(Name = "API接口")]
        apiuser=2
    }
}
