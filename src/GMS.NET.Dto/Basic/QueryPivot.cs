﻿using System.ComponentModel;

namespace GMS.NET.Dto.Basic
{
    /// <summary>
    /// 表格查询辅助类
    /// </summary>
    public class QueryPivot
    {
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 20;
        public string? PageSort { get; set; }
        public bool IsPage { get; set; }
        public List<WhereModel>? WhereModel { get; set; }
    }
    /// <summary>
    /// 表格查询辅助类
    /// </summary>
    public class QueryPivot<T>
    {
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 20;
        public string? PageSort { get; set; }
        public bool IsPage { get; set; }
        public List<WhereModel>? WhereModel { get; set; }
        public T? OtherSearches { get; set; }
    }
    public class WhereModel
    {
        public string? FieldName { get; set; }
        public object? FieldValue { get; set; }
        public Conditional? ConditionalType { get; set; }
        public CSharpType? CSharpTypeName { get; set; }
    }

    /// <summary>
    /// 表格数据
    /// </summary>
    /// <typeparam name="TItem"></typeparam>
    public class TabularData<TItem>
    {
        public int page_Total { get; set; }
        public List<TItem>? Items { get; set; }
    }
    /// <summary>
    /// 字段类型
    /// </summary>
    public enum CSharpType
    {
        String,
        Int,
        Decimal,
        DateTime
    }

    /// <summary>
    /// 关系运算符
    /// </summary>
    public enum Conditional
    {
        /// <summary>
        /// 等于
        /// </summary>
        [Description("等于")]
        Equal = 0,

        /// <summary>
        /// 不等于
        /// </summary>
        [Description("不等于")]
        NotEqual = 10,

        /// <summary>
        /// 大于
        /// </summary>
        [Description("大于")]
        GreaterThan = 2,

        /// <summary>
        /// 大于等于
        /// </summary>
        [Description("大于等于")]
        GreaterThanOrEqual = 3,

        /// <summary>
        /// 小于
        /// </summary>
        [Description("小于")]
        LessThan = 4,

        /// <summary>
        /// 小于等于
        /// </summary>
        [Description("小于等于")]
        LessThanOrEqual = 5,

        /// <summary>
        /// 包含
        /// </summary>
        [Description("包含")]
        Like = 1,

        /// <summary>
        /// 不包含
        /// </summary>
        [Description("不包含")]
        NoLike = 13,

        /// <summary>
        /// 多选
        /// 例:X,Y,Z 
        /// </summary>
        [Description("多选")]
        In = 6,

        /// <summary>
        /// 是null或者''
        /// </summary>
        [Description("是null或者''")]
        IsNullOrEmpty = 11,

        /// <summary>
        /// value不等于null <> 或者 value等于null  is not null
        /// </summary>
        [Description("是null或者''")]
        IsNot = 12
    }
}
