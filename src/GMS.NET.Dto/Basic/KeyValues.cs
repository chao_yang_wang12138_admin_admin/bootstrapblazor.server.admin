﻿namespace GMS.NET.Dto.Basic
{
    public class KeyValues
    {
        public string? Key { get; set; }
        public object? Value { get; set; }
    }
}
