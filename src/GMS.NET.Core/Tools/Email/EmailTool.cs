﻿using System.Net.Mail;
using System.Net;
using MimeKit;

namespace GMS.NET.Core.Tools.Email
{
    /// <summary>
    /// 邮件操作类
    /// </summary>
    public class EmailTool
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sysNetEmail"></param>
        public static void SysNetEmail(SysNetEmail sysNetEmail)
        {
            // 发送者的邮箱配置
            string senderEmail = sysNetEmail.senderEmail!;
            string senderName = sysNetEmail.senderName!;
            string senderPassword = sysNetEmail.senderPassword!;

            // 接收者的邮箱
            string receiverEmail = sysNetEmail.receiverEmail!;

            // 邮件内容
            string subject = sysNetEmail.subject!;
            string body = sysNetEmail.body!;

            // SMTP服务器配置，例如使用Gmail的SMTP服务器
            SmtpClient smtpClient = new SmtpClient
            {
                Host = sysNetEmail.smtpClient!.Host,
                Port = sysNetEmail.smtpClient.Port,
                EnableSsl = sysNetEmail.smtpClient.EnableSsl,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(senderEmail, senderPassword)
            };

            // 创建邮件
            MailMessage mailMessage = new MailMessage
            {
                From = new MailAddress(senderEmail, senderName),
                Subject = subject,
                Body = body
            };
            mailMessage.To.Add(receiverEmail);

            // 发送邮件
            smtpClient.Send(mailMessage);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mailKitEmail"></param>
        public static void MailKitEmail(MailKitEmail mailKitEmail)
        {
            using (var smtp = new MailKit.Net.Smtp.SmtpClient())
            {
                MimeMessage mail = new MimeMessage();
                mail.From.Add(new MailboxAddress(mailKitEmail.senderName, mailKitEmail.senderEmail));
                if (mailKitEmail.receiverEmails != null && mailKitEmail.receiverEmails!.Any())
                {
                    for (int i = 0; i < mailKitEmail.receiverEmails!.Count(); i++)
                    {
                        mail.To.Add(new MailboxAddress("收件人" + i.ToString(), mailKitEmail.receiverEmails![i]));
                    }
                    mail.Subject = mailKitEmail.subject;
                    if (string.IsNullOrEmpty(mailKitEmail.body))
                    {
                        throw new Exception("发送内容不能为空");
                    }
                    var Html = new TextPart(MimeKit.Text.TextFormat.Html)
                    {
                        Text = mailKitEmail.body + empletstr()
                    };
                    mail.Body = Html;


                    smtp.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    smtp.Connect(mailKitEmail.smtpClient!.Host, mailKitEmail.smtpClient!.Port, mailKitEmail.smtpClient!.EnableSsl);
                    smtp.Authenticate(mailKitEmail.senderEmail, mailKitEmail.senderPassword);
                    smtp.Timeout = 600000;
                    string res = smtp.Send(mail);
                    smtp.Disconnect(true);
                }
            }
        }
        private static string empletstr()
        {
            return "<br/><span style='color: darkgrey;font-size: 13px;font-weight:300;'>----------------------------------------------------------------------------------------------------------------------</span><br/><span style='color: darkgrey;font-size: 13px;font-weight:300;'>登录地址:https://xxxxx.com/</span><br/><span  style='color: darkgrey;font-size: 13px;font-weight:300;'>注:邮件来自GMSNET系统邮箱，当系统检测到您的账号触发机制时，会自动发送此提醒邮件给您，此信息无需回复。</span>";
        }
    }
}
