﻿namespace GMS.NET.Core.Tools.Email
{
    /// <summary>
    /// SysNetEmail配置
    /// </summary>
    public class SysNetEmail
    {
        /// <summary>
        /// 
        /// </summary>
        public string? senderEmail { get; set; } = "wangchaoyang@gm-supply.cn";
        /// <summary>
        /// 
        /// </summary>
        public string? senderName { get; set; } = "GMSNET author";
        /// <summary>
        /// 
        /// </summary>
        public string? senderPassword { get; set; } = "wcyy.1234";
        /// <summary>
        /// 
        /// </summary>
        public string? receiverEmail { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string? subject { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string? body { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public smtpClient? smtpClient { get; set; } = new();
    }
    /// <summary>
    /// 
    /// </summary>
    public class smtpClient
    {
        /// <summary>
        /// 
        /// </summary>
        public string Host { get; set; } = "smtp.qiye.aliyun.com";
        /// <summary>
        /// 
        /// </summary>
        public int Port { get; set; } = 465;
        /// <summary>
        /// 
        /// </summary>
        public bool EnableSsl { get; set; } = true;
    }
}
