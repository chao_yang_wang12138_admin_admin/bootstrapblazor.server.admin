﻿using GMS.NET.Core.Entitys.System;
using GMS.NET.Dto.System.AccRoles.Output;
using GMS.NET.Dto.System.Layout.Ouput;

namespace GMS.NET.Core.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public class MapperTool : IRegister
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<GMSNet_Resouce, MenuItemSync>()
               .Map(dest => dest.Text, src => src.Res_Name)
               .Map(dest => dest.Icon, src => src.Res_Icon)
               .Map(dest => dest.CssClass, src => src.Res_CssClass)
               .Map(dest => dest.Url, src => src.Res_Url)
               .Map(dest => dest.Items, src => src.Children);
        }
    }
}
