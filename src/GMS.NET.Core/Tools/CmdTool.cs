﻿
using System.Diagnostics;

namespace GMS.NET.Core.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public class CmdTool
    {
        /// <summary>
        /// 开始执行
        /// </summary>
        /// <param name="cmdstring"></param>
        /// <param name="cmdfile"></param>
        public static void Start(string cmdstring,string cmdfile= @"powershell.exe")
        {
            var processInstall = new Process();
            processInstall.StartInfo.FileName = cmdfile;
            processInstall.StartInfo.Arguments = cmdstring;
            processInstall.Start();
            processInstall.WaitForExit();
        }
    }
}
