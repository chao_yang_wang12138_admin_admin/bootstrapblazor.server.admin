﻿using GMS.NET.Dto.Basic;

namespace GMS.NET.Core.Tools
{
    /// <summary>
    /// 数据查询条件工具
    /// </summary>
    public static class QueryPivotTool
    {
        /// <summary>
        /// 获取数据查询条件
        /// </summary>
        /// <param name="wheres"></param>
        /// <returns></returns>
        public static List<IConditionalModel> GetSearches(this List<WhereModel> wheres)
        {
            var result = new List<IConditionalModel>();
            if (wheres is not null)
            {
                var rator = wheres.GetEnumerator();
                while (rator.MoveNext())
                {
                    var model = rator.Current;
                    if (model.FieldValue is not null)
                        result.Add(new ConditionalModel()
                        {
                            FieldName = model.FieldName,
                            FieldValue = model.FieldValue.ToString(),
                            CSharpTypeName = model.CSharpTypeName.ToString(),
                            ConditionalType = (ConditionalType)model.ConditionalType!
                        });
                }
            }
            return result;
        }
    }
}
