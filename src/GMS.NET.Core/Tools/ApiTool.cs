﻿using GMS.NET.Core.Options;
using Microsoft.AspNetCore.Http;

namespace GMS.NET.Core.Tools
{
    /// <summary>
    /// api处理类
    /// </summary>
    public class ApiTool
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetIp()
        {
            var FrontOptions = App.GetOptions<FrontendOptions>();
            var remoteIPv4 = App.HttpContext.GetRemoteIpAddressToIPv4();
            if (FrontOptions.Proxy) return remoteIPv4;
            var FrontEndHost = FrontOptions.Ips;
            if (FrontEndHost!.Any(x => x == remoteIPv4))
                return App.HttpContext.Request.Headers["X-Forwarded-For"].ToString();
            else
                return remoteIPv4;
        }
    }
}
