﻿using Furion.ConfigurableOptions;

namespace GMS.NET.Core.Users
{
    /// <summary>
    /// 用户管理员配置
    /// </summary>
    public class UserAdminOptions : IConfigurableOptions
    {
        /// <summary>
        /// 管理员角色Id
        /// </summary>
        public string? RoleId { get; set; }
        /// <summary>
        /// 管理员客户Id
        /// </summary>
        public string? CustsId { get; set; }
    }
    /// <summary>
    /// 用户权限常量
    /// </summary>
    public class UserAuthority
    {
        /// <summary>
        /// 管理员用户名
        /// </summary>
        public const string Administratoruser = $"GMSNetAdministrator";
    }
}