﻿using GMS.NET.Const;
using System.Security.Claims;

namespace GMS.NET.Core.Users
{
    /// <summary>
    /// 获取用户信息
    /// </summary>
    public class UserInfo
    {
        private static readonly UserAdminOptions AdminInfo = App.GetOptions<UserAdminOptions>();
        /// <summary>
        /// 用户管理员
        /// </summary>
        public static bool UserAdmin => RoleId == AdminInfo.RoleId && CusId == AdminInfo.CustsId;
        /// <summary>
        /// 角色Id
        /// </summary>
        public static string RoleId => GetUserInfo(UserClaimConst.RoleId);
        /// <summary>
        /// 用户Id
        /// </summary>
        public static string UserId => GetUserInfo(UserClaimConst.UserId);
        /// <summary>
        /// 客户Id
        /// </summary>
        public static string CusId => GetUserInfo(UserClaimConst.CusId);
        private static string GetUserInfo(string claim)
        {
            if (App.User is null)
                if (claim == UserClaimConst.CusId ||
                    claim == UserClaimConst.RoleId)
                    return Guid.Empty.ToString();
                else
                    return UserAuthority.Administratoruser;
            else
                return App.User.FindFirstValue(claim);
        }
    }
    /// <summary>
    /// 用户Saas连接信息
    /// </summary>
    public class SaasUserInfo
    {
        /// <summary>
        /// 客户Id
        /// </summary>
        public string? CusId { get; set; }
        /// <summary>
        /// 连接字符串
        /// </summary>
        public string? Connection { get; set; }
    }
}
