﻿using MiniExcelLibs.Attributes;

namespace GMS.NET.Core.Entitys.Excel.Basic
{
    /// <summary>
    /// 
    /// </summary>
    public class ExcelBasicEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [ExcelColumnName("检验说明")]
        public string? CheckMessage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [ExcelColumnName("检验状态")]
        public bool CheckStatus { get; set; }
    }
}
