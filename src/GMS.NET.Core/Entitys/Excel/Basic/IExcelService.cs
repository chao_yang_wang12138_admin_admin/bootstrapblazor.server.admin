﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMS.NET.Core.Entitys.Excel.Basic
{
    /// <summary>
    /// excel服务类
    /// </summary>
    public interface IExcelService
    {
        /// <summary>
        /// 检验数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="func"></param>
        /// <returns></returns>
        Task CheckData<T>(Func<MemoryStream,Task<(List<T> dataList, bool checkStatus)>> func);

        /// <summary>
        /// 导入数据
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        Task ImportData(Func<MemoryStream,Task< bool>> func);
    }
}
