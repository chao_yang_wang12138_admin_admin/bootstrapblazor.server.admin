﻿using MiniExcelLibs.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GMS.NET.Core.Entitys.Excel.Basic
{
    /// <summary>
    /// 
    /// </summary>
    public static class Crossovertool
    {
        /// <summary>
        /// 根据获取excel特性转换为DictionaryList
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="values"></param>
        /// <returns></returns>
        public static List<Dictionary<string, object>> ConvertGetExcelAttrToDictionaryList<T>(this List<T> values)
        {
            var dictionaryList = new List<Dictionary<string, object>>();

            foreach (var item in values)
            {
                var dictionary = new Dictionary<string, object>();
                var itemType = item!.GetType();
                var properties = itemType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

                foreach (var property in properties)
                {
                    var propertyName = property.GetCustomAttribute<ExcelColumnNameAttribute>()!.ExcelColumnName;
                    var propertyValue = property.GetValue(item);
                    dictionary[propertyName] = propertyValue!;
                }

                dictionaryList.Add(dictionary);
            }

            return dictionaryList;
        }
    }
}
