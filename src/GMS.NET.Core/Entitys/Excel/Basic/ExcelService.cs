﻿using Microsoft.Extensions.Caching.Memory;
using MiniExcelLibs;

namespace GMS.NET.Core.Entitys.Excel.Basic
{
    /// <summary>
    /// excel服务类
    /// </summary>
    public class ExcelService : IExcelService
    {

        private readonly IMemoryCache _memoryCache = App.GetService<IMemoryCache>();

        /// <summary>
        /// 检测数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="func"></param>
        /// <returns></returns>
        public async Task CheckData<T>(Func<MemoryStream,Task<(List<T> dataList,bool checkStatus)>> func)
        {
            var Stream = _memoryCache.Get<MemoryStream>("TestUserId");
            var _func=await func(Stream);
            var _Stream = new MemoryStream();
            await _Stream.SaveAsAsync(_func.dataList.ConvertGetExcelAttrToDictionaryList());
            _Stream.Position = 0;
            _memoryCache.Set("TestUserId", _Stream, TimeSpan.FromHours(1));
            if (!_func.checkStatus) throw new Exception("数据验证失败");
        }
        /// <summary>
        /// 导入数据
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        public async Task ImportData(Func<MemoryStream,Task<bool>> func)
        {
            var Stream = _memoryCache.Get<MemoryStream>("TestUserId");
            var _func =await func(Stream);
            if ( _func) _memoryCache.Remove("TestUserId");
        }
    }
}
