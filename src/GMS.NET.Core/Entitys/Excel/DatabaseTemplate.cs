﻿using GMS.NET.Core.Entitys.Excel.Basic;
using MiniExcelLibs.Attributes;

namespace GMS.NET.Core.Entitys.Excel
{
    /// <summary>
    /// 数据库excel模板
    /// </summary>
    public class DatabaseTemplate: ExcelBasicEntity
    {
        /// <summary>
        /// 序号
        /// </summary>
        [ExcelColumnName("序号")]
        public int Serialnumber { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [ExcelColumnName("名称")]
        public string? Name { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [ExcelColumnName("描述")]
        public string? Descr { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        [ExcelColumnName("类型")]
        public string? Type { get; set; }
        /// <summary>
        /// 关键字
        /// </summary>
        [ExcelColumnName("关键字")]
        public string? Keyword { get; set; }
        /// <summary>
        /// 允许为空
        /// </summary>
        [ExcelColumnName("允许为空")]
        public string? IsNull { get; set; }
        /// <summary>
        /// 例
        /// </summary>
        [ExcelColumnName("例")]
        public string? Example { get; set; }
    }
}
