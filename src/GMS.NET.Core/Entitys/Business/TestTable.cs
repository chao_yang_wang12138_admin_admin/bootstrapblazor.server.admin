namespace GMS.NET.Core.Entitys.Business
{
	///<summary>
	///测试表格
	///</summary>
	public partial class TestTable
	{
		/// <summary>
		///
		/// </summary>
		public void SetAddUser()
		{
			this.Creator = UserInfo.UserId;
			this.Modifier = UserInfo.UserId;
			this.Creationtime = DateTime.Now;
			this.Modifytime = DateTime.Now;
		}

		/// <summary>
		///
		/// </summary>
		public void SetUpdateUser()
		{
			this.Modifier = UserInfo.UserId;
			this.Modifytime = DateTime.Now;
		}

		/// <summary>
		///
		/// </summary>
		public TestTable() { }

		/// <summary>
		/// Desc:Guid
		/// </summary>
		[SugarColumn(IsPrimaryKey = true)]
		public Guid Id { get; set; }

		/// <summary>
		/// Desc:测试字段
		/// </summary>
		[SugarColumn(Length = 100)]
		public string? Test { get; set; }

		/// <summary>
		/// Desc:测试字段2
		/// </summary>
		[SugarColumn(IsNullable = true)]
		public DateTime? Test2 { get; set; }

		/// <summary>
		/// Desc:创建时间
		/// </summary>
		public DateTime Creationtime { get; set; }

		/// <summary>
		/// Desc:创建人
		/// </summary>
		[SugarColumn(Length = 100)]
		public string? Creator { get; set; }

		/// <summary>
		/// Desc:修改时间
		/// </summary>
		public DateTime Modifytime { get; set; }

		/// <summary>
		/// Desc:修改人
		/// </summary>
		[SugarColumn(Length = 100)]
		public string? Modifier { get; set; }

	}
}
