﻿namespace GMS.NET.Core.Entitys.System
{
    ///<summary>
    ///GMSNet资源信息表
    ///</summary>
    [SugarIndex("GMSNet_Resouce_Name", nameof(Res_Name), OrderByType.Desc, true)]
    public partial class GMSNet_Resouce
    {
        /// <summary>
        /// 
        /// </summary>
        public void SetAddUser()
        {
            this.Creator = UserInfo.UserId;
            this.Modifier = UserInfo.UserId;
            this.Creationtime = DateTime.Now;
            this.Modifytime = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetUpdateUser()
        {
            this.Modifier = UserInfo.UserId;
            this.Modifytime = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        public GMSNet_Resouce() { }
        /// <summary>
        /// Desc:系统资源Guid
        /// </summary>           
        [SugarColumn(IsPrimaryKey = true)]
        public Guid Id { get; set; }

        /// <summary>
        /// Desc:资源类型
        /// </summary>           
        public int Res_Type { get; set; }

        /// <summary>
        /// Desc:资源名称(类名/方法注释)
        /// </summary>   
        [SugarColumn(Length = 150)]
        public string? Res_Name { get; set; }

        /// <summary>
        /// Desc:资源图标
        /// </summary>    
        [SugarColumn(Length = 50, IsNullable = true)]
        public string? Res_Icon { get; set; }

        /// <summary>
        /// Desc:资源样式
        /// </summary>           
        [SugarColumn(Length = 80, IsNullable = true)]
        public string? Res_CssClass { get; set; }

        /// <summary>
        /// Desc:资源状态
        /// </summary>           
        public int? Res_Disabled { get; set; }

        /// <summary>
        /// Desc:资源地址
        /// </summary>           
        [SugarColumn(Length = 150, IsNullable = true)]
        public string? Res_Url { get; set; }

        /// <summary>
        /// Desc:父级资源
        /// </summary>           
        public Guid Res_ParentId { get; set; }

        /// <summary>
        /// Desc:资源排序
        /// </summary>           
        public int Res_Sort { get; set; }

        /// <summary>
        /// Desc:代码名称(接口名称/类名)选填
        /// </summary>           
        [SugarColumn(Length = 100, IsNullable = true)]
        public string? Res_CodeName { get; set; }

        /// <summary>
        /// Desc:生成api方法需要指定的代码名称项(选填)
        /// </summary>           
        [SugarColumn(Length = 100, IsNullable = true)]
        public string? Res_CsFileName { get; set; }

        /// <summary>
        /// Desc:生成方法类需要指定的目录名称默认在服务器层Service文件夹下(选填)
        /// </summary>           
        [SugarColumn(Length = 100, IsNullable = true)]
        public string? Res_CsFileCatalog { get; set; }

        /// <summary>
        /// Desc:资源备注
        /// </summary>           
        [SugarColumn(Length = 500, IsNullable = true)]
        public string? Res_Remark { get; set; }

        /// <summary>
        /// Desc:创建时间
        /// </summary>    
        public DateTime Creationtime { get; set; }

        /// <summary>
        /// Desc:创建人
        /// </summary>           
        [SugarColumn(Length = 100)]
        public string? Creator { get; set; }

        /// <summary>
        /// Desc:修改时间
        /// </summary>         
        public DateTime Modifytime { get; set; }

        /// <summary>
        /// Desc:修改人
        /// </summary>     
        [SugarColumn(Length = 100)]
        public string? Modifier { get; set; }

        /// <summary>
        /// 菜单列表
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public List<GMSNet_Resouce>? Children { get; set; }
        /// <summary>
        /// 资源权限列表
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public Guid? Auth_ResId { get; set; }
    }
}
