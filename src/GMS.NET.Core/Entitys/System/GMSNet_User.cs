﻿namespace GMS.NET.Core.Entitys.System
{
    ///<summary>
    ///GMSNet用户信息表
    ///</summary>
    [SugarIndex("GMSNet_User_Account", nameof(User_Account), OrderByType.Desc, true)]
    [SugarIndex("GMSNet_User_Phone", nameof(User_Phone), OrderByType.Desc, true)]
    [SugarIndex("GMSNet_User_Email", nameof(User_Email), OrderByType.Desc, true)]
    public partial class GMSNet_User
    {
        /// <summary>
        /// 
        /// </summary>
        public void SetAddUser()
        {
            this.Creator = UserInfo.UserId;
            this.Modifier = UserInfo.UserId;
            this.Creationtime = DateTime.Now;
            this.Modifytime = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetUpdateUser()
        {
            this.Modifier = UserInfo.UserId;
            this.Modifytime = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        public GMSNet_User() { }
        /// <summary>
        /// Desc:主键Guid
        /// </summary>           
        [SugarColumn(IsPrimaryKey = true)]
        public Guid Id { get; set; }

        /// <summary>
        /// Desc:用户账号
        /// </summary>
        [SugarColumn(Length = 100)]
        public string? User_Account { get; set; }

        /// <summary>
        /// Desc:用户密码
        /// </summary>     
        [SugarColumn(Length = 255)]
        public string? User_PassWord { get; set; }

        /// <summary>
        /// Desc:用户名称
        /// </summary>  
        [SugarColumn(Length = 100)]
        public string? User_Name { get; set; }

        /// <summary>
        /// Desc:所属组织
        /// </summary>           
        public Guid User_CusId { get; set; }

        /// <summary>
        /// Desc:手机号码
        /// </summary>    
        [SugarColumn(Length = 11, IsNullable = true)]
        public string? User_Phone { get; set; }

        /// <summary>
        /// Desc:邮箱地址
        /// </summary> 
        [SugarColumn(Length = 255, IsNullable = true)]
        public string? User_Email { get; set; }

        /// <summary>
        /// Desc:用户角色
        /// </summary>           
        public Guid User_RoleId { get; set; }

        /// <summary>
        /// Desc:用户状态1正常0禁用
        /// </summary>           
        public int User_Status { get; set; }

        /// <summary>
        /// Desc:用户类型1系统用户2API接口
        /// </summary>           
        public int User_Type { get; set; }

        /// <summary>
        /// Desc:创建时间
        /// </summary>           
        public DateTime? Creationtime { get; set; }

        /// <summary>
        /// Desc:创建人
        /// </summary>       
        [SugarColumn(Length = 100)]
        public string? Creator { get; set; }

        /// <summary>
        /// Desc:修改时间
        /// </summary>           
        public DateTime Modifytime { get; set; }

        /// <summary>
        /// Desc:修改人
        /// </summary>    
        [SugarColumn(Length = 100)]
        public string? Modifier { get; set; }

        /// <summary>
        /// Desc:用户备注
        /// </summary>  
        [SugarColumn(Length = 500, IsNullable = true)]
        public string? User_Remark { get; set; }

        /// <summary>
        /// Desc:是否在注册客户时已了解注意事项0未确认1已确认
        /// </summary>  
        [SugarColumn(IsNullable = true,DefaultValue ="0")]
        public int User_OperCustsNotes { get; set; }
    }
}
