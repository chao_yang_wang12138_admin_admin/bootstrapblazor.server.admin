﻿namespace GMS.NET.Core.Entitys.System
{
    ///<summary>
    ///GMSNet用户记录日志表
    ///</summary>
    [SplitTable(SplitType.Year)]
    [SugarTable("GMSNet_LOG_{year}{month}{day}")]
    public partial class GMSNet_LOG
    {
        /// <summary>
        /// 
        /// </summary>
        public void SetAddUser()
        {
            this.Creator = UserInfo.UserId;
            this.Creationtime = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        public GMSNet_LOG() { }
        /// <summary>
        /// Desc:日志Guid
        /// </summary>           
        [SugarColumn(IsPrimaryKey = true)]
        public Guid Id { get; set; }

        /// <summary>
        /// Desc:日志内容
        /// </summary>           
        [SugarColumn(IsNullable = true, Length = 3500)]
        public string? LOG_Content { get; set; }

        /// <summary>
        /// Desc:日志类别
        /// </summary>  
        [SugarColumn(IsNullable = true, Length = 100)]
        public string? LOG_LogLevel { get; set; }

        /// <summary>
        /// Desc:创建时间
        /// </summary> 
        [SplitField]
        public DateTime Creationtime { get; set; }

        /// <summary>
        /// Desc:创建人
        /// </summary>      
        [SugarColumn(Length = 200)]
        public string? Creator { get; set; }

    }
}
