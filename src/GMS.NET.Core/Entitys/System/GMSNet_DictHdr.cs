﻿namespace GMS.NET.Core.Entitys.System
{
    ///<summary>
    ///GMSNet字典表主表
    ///</summary>
    public partial class GMSNet_DictHdr
    {
        /// <summary>
        /// 
        /// </summary>
        public void SetAddUser()
        {
            this.Creator = UserInfo.UserId;
            this.Modifier = UserInfo.UserId;
            this.Creationtime = DateTime.Now;
            this.Modifytime = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetUpdateUser()
        {
            this.Modifier = UserInfo.UserId;
            this.Modifytime = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        public GMSNet_DictHdr() { }
        /// <summary>
        /// Desc:字典主表Guid
        /// </summary>           
        [SugarColumn(IsPrimaryKey = true)]
        public Guid Id { get; set; }

        /// <summary>
        /// Desc:键
        /// </summary>        
        [SugarColumn(Length = 100)]
        public string? DictHdr_Key { get; set; }

        /// <summary>
        /// Desc:值
        /// </summary>        
        [SugarColumn(Length = 255)]
        public string? DictHdr_Value { get; set; }

        /// <summary>
        /// Desc:创建时间
        /// </summary>           
        public DateTime Creationtime { get; set; }

        /// <summary>
        /// Desc:创建人
        /// </summary>           
        [SugarColumn(Length = 100)]
        public string? Creator { get; set; }

        /// <summary>
        /// Desc:创建时间
        /// </summary>           
        public DateTime Modifytime { get; set; }

        /// <summary>
        /// Desc:创建人
        /// </summary>           
        [SugarColumn(Length = 100)]
        public string? Modifier { get; set; }

        /// <summary>
        /// Desc:备注
        /// </summary>   
        [SugarColumn(IsNullable = true, Length = 500)]
        public string? DictHdr_Remark { get; set; }

    }
}
