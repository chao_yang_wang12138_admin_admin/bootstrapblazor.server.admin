﻿namespace GMS.NET.Core.Entitys.System
{
    ///<summary>
    ///GMSNet角色信息表
    ///</summary>
    [SugarIndex("GMSNet_Role_Name", nameof(Role_Name), OrderByType.Desc, true)]
    public partial class GMSNet_Role
    {
        /// <summary>
        /// 
        /// </summary>
        public void SetAddUser()
        {
            this.Creator = UserInfo.UserId;
            this.Modifier = UserInfo.UserId;
            this.Creationtime = DateTime.Now;
            this.Modifytime = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetUpdateUser()
        {
            this.Modifier = UserInfo.UserId;
            this.Modifytime = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        public GMSNet_Role() { }
        /// <summary>
        /// Desc:角色Guid
        /// </summary>           
        [SugarColumn(IsPrimaryKey = true)]
        public Guid Id { get; set; }

        /// <summary>
        /// Desc:角色名称
        /// </summary> 
        [SugarColumn(Length = 100)]
        public string? Role_Name { get; set; }

        /// <summary>
        /// Desc:角色所属客户
        /// </summary>  
        public Guid Role_Custs { get; set; }

        /// <summary>
        /// Desc:角色状态1正常2禁用
        /// </summary>           
        public int Role_Status { get; set; }

        /// <summary>
        /// Desc:角色备注
        /// </summary>           
        [SugarColumn(Length = 500, IsNullable = true)]
        public string? Role_Remark { get; set; }

        /// <summary>
        /// Desc:创建时间
        /// </summary>           
        public DateTime Creationtime { get; set; }

        /// <summary>
        /// Desc:创建人
        /// </summary>     
        [SugarColumn(Length = 100)]
        public string? Creator { get; set; }

        /// <summary>
        /// Desc:修改时间
        /// </summary>           
        public DateTime Modifytime { get; set; }

        /// <summary>
        /// Desc:修改人
        /// </summary>           
        [SugarColumn(Length = 100)]
        public string? Modifier { get; set; }

    }
}
