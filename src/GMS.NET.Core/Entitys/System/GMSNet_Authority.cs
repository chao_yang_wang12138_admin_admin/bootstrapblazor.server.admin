﻿namespace GMS.NET.Core.Entitys.System
{
    ///<summary>
    ///GMSNet权限信息表
    ///</summary>
    public partial class GMSNet_Authority
    {
        /// <summary>
        /// 
        /// </summary>
        public void SetAddUser()
        {
            this.Creator = UserInfo.UserId;
            this.Modifier = UserInfo.UserId;
            this.Creationtime = DateTime.Now;
            this.Modifytime = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetUpdateUser()
        {
            this.Modifier = UserInfo.UserId;
            this.Modifytime = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        public GMSNet_Authority() { }
        /// <summary>
        /// Desc:权限Guid
        /// </summary> 
        [SugarColumn(IsPrimaryKey = true)]
        public Guid Id { get; set; }

        /// <summary>
        /// Desc:角色Id
        /// </summary> 
        public Guid Auth_RoleId { get; set; }

        /// <summary>
        /// Desc:资源Id
        /// </summary>
        public Guid Auth_ResId { get; set; }

        /// <summary>
        /// Desc:权限说明
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500)]
        public string? Auth_Remark { get; set; }

        /// <summary>
        /// Desc:创建时间
        /// </summary> 
        public DateTime Creationtime { get; set; }

        /// <summary>
        /// Desc:创建人
        /// </summary>
        [SugarColumn(Length = 100)]
        public string? Creator { get; set; }

        /// <summary>
        /// Desc:修改时间
        /// </summary>
        public DateTime Modifytime { get; set; }

        /// <summary>
        /// Desc:修改人
        /// </summary>       
        [SugarColumn(Length = 100)]
        public string? Modifier { get; set; }

    }
}
