﻿namespace GMS.NET.Core.Entitys.System
{
    ///<summary>
    ///GMSNet客户信息表
    ///</summary>
    [SugarIndex("GMSNet_Customer_Cust_Name", nameof(Cust_Name), OrderByType.Desc, true)]
    [SugarIndex("GMSNet_Customer_Cust_FullName", nameof(Cust_FullName), OrderByType.Desc, true)]
    [SugarIndex("GMSNet_Customer_Phone", nameof(Cust_Phone), OrderByType.Desc, true)]
    [SugarIndex("GMSNet_Customer_Email", nameof(Cust_Email), OrderByType.Desc, true)]
    [SugarIndex("GMSNet_Customer_Connection", nameof(Cust_Connection), OrderByType.Desc, true)]
    public partial class GMSNet_Customer
    {
        /// <summary>
        /// 
        /// </summary>
        public void SetAddUser()
        {
            this.Creator = UserInfo.UserId;
            this.Modifier = UserInfo.UserId;
            this.Creationtime = DateTime.Now;
            this.Modifytime = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetUpdateUser()
        {
            this.Modifier = UserInfo.UserId;
            this.Modifytime = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        public GMSNet_Customer() { }
        /// <summary>
        /// Desc:客户Guid
        /// </summary>           
        [SugarColumn(IsPrimaryKey = true)]
        public Guid Id { get; set; }

        /// <summary>
        /// Desc:客户简称
        /// </summary>
        [SugarColumn(Length = 150)]
        public string? Cust_Name { get; set; }

        /// <summary>
        /// Desc:客户序列
        /// </summary>
        [SugarColumn(IsIdentity =true)]
        public int Cust_Seq { get; set; }

        /// <summary>
        /// Desc:客户全称
        /// </summary>        
        [SugarColumn(Length = 255)]
        public string? Cust_FullName { get; set; }

        /// <summary>
        /// Desc:客户数据库
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 255)]
        public string? Cust_Connection { get; set; }

        /// <summary>
        /// Desc:客户电话
        /// </summary>          
        [SugarColumn(IsNullable = true, Length = 20)]
        public string? Cust_Phone { get; set; }

        /// <summary>
        /// Desc:客户类型1默认类型2VIP
        /// </summary>        
        public int Cust_Type { get; set; }

        /// <summary>
        /// Desc:客户地址
        /// </summary>     

        [SugarColumn(IsNullable = true, Length = 255)]
        public string? Cust_Address { get; set; }

        /// <summary>
        /// Desc:客户状态1正常0禁用2删除
        /// </summary>           
        public int Cust_Status { get; set; }

        /// <summary>
        /// Desc:删除时间
        /// </summary> 
        [SugarColumn(IsNullable =true)]
        public DateTime? Cust_Deltime { get; set; }

        /// <summary>
        /// Desc:客户邮箱
        /// </summary>          
        [SugarColumn(IsNullable = true,Length = 255)]
        public string? Cust_Email { get; set; }

        /// <summary>
        /// Desc:创建时间
        /// </summary>           
        public DateTime Creationtime { get; set; }

        /// <summary>
        /// Desc:创建人
        /// </summary>           
        [SugarColumn(Length = 100)]
        public string? Creator { get; set; }

        /// <summary>
        /// Desc:修改时间
        /// </summary>           
        public DateTime Modifytime { get; set; }

        /// <summary>
        /// Desc:修改人
        /// </summary>      
        [SugarColumn(Length = 100)]
        public string? Modifier { get; set; }

        /// <summary>
        /// Desc:客户备注
        /// </summary>           
        [SugarColumn(IsNullable = true, Length = 500)]
        public string? Cust_Remark { get; set; }

    }
}
