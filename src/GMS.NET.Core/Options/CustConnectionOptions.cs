﻿using Furion.ConfigurableOptions;
namespace GMS.NET.Core.Options
{
    /// <summary>
    /// 客户数据库连接配置
    /// </summary>
    public class CustConnectionOptions: IConfigurableOptions
    {
        /// <summary>
        /// 服务主机
        /// </summary>
        public string? Server { get; set; }
        /// <summary>
        /// 账号
        /// </summary>
        public string? User_Id { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string? Password { get; set; }
    }
}
