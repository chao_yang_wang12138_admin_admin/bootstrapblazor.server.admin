﻿using Furion.ConfigurableOptions;

namespace GMS.NET.Core.Options
{
    /// <summary>
    /// 应用程序配置
    /// </summary>
    public class ApplicationOptions: IConfigurableOptions
    {
        /// <summary>
        /// api请求验证
        /// </summary>
        public ApiValidation? ApiValidation { get; set; }
        /// <summary>
        /// 系统缓存
        /// </summary>
        public SystemCache? SystemCache { get; set; }
    }
    /// <summary>
    /// api请求验证
    /// </summary>
    public class ApiValidation
    {
        /// <summary>
        /// 时间范围/秒
        /// </summary>
        public int TimeRange { get; set; }
        /// <summary>
        /// 系统密钥
        /// </summary>
        public string? ServerKey { get; set; }
    }
    /// <summary>
    /// 系统缓存
    /// </summary>
    public class SystemCache
    {
        /// <summary>
        /// 缓存名称
        /// </summary>
        public string? Name { get; set; }
        /// <summary>
        /// 缓存服务器(可选)
        /// </summary>
        public string? ConnectString { get; set; }
    }
}
