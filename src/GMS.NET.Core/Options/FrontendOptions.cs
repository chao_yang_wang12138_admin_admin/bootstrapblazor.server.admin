﻿using Furion.ConfigurableOptions;

namespace GMS.NET.Core.Options
{
    /// <summary>
    /// 前端配置项
    /// </summary>
    public class FrontendOptions: IConfigurableOptions
    {
        /// <summary>
        /// 是否已配置反向代理
        /// </summary>
        public bool Proxy { get; set; }
        /// <summary>
        /// 前端Ip,用于记录日志时判断是否是用户真实Ip
        /// </summary>
        public string[]? Ips { get;set; }
    }
}
