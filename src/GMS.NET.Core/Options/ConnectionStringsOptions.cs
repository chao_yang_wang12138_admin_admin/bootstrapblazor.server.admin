﻿using Furion.ConfigurableOptions;

namespace GMS.NET.Core.Options
{
    /// <summary>
    /// 用户连接字符串
    /// </summary>
    public class ConnectionStringsOptions: IConfigurableOptions
    {
        /// <summary>
        /// 主数据库连接字符串
        /// </summary>
        public string? MasterDataBank { get;set; }   
        /// <summary>
        /// 连接Id
        /// </summary>
        public string? ConfigId { get;set; }
    }
}
