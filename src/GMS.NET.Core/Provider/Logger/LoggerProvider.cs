﻿using Microsoft.Extensions.Logging;
namespace GMS.NET.Core.Provider.Logger
{
    /// <summary>
    /// 
    /// </summary>
    public class LoggerProvider : ILoggerProvider
    {
        private readonly  string? _logFilePath;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logFilePath"></param>
        public LoggerProvider(string logFilePath)
        {
            _logFilePath = logFilePath;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        public ILogger CreateLogger(string categoryName)
        {
            return new LoggerExtensions(_logFilePath);
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            // 这里释放任何资源，如果有的话
        }
    }
}
