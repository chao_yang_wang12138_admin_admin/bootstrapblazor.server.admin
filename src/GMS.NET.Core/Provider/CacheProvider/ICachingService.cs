﻿namespace GMS.NET.Core.Provider.CacheProvider
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICachingService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<T> GetAsync<T>(string key);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        Task<string> GetStringAsync(string key);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="absoluteExpirationRelativeToNow"></param>
        /// <returns></returns>
        Task SetAsync<T>(string key,T value,TimeSpan absoluteExpirationRelativeToNow);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="absoluteExpirationRelativeToNow"></param>
        void Set<T>(string key, T value, TimeSpan absoluteExpirationRelativeToNow);
        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key"></param>
        void Remove(string key);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<bool> AnyAsync(string key);
    }
}
