﻿using Furion.DependencyInjection;
using GMS.NET.Core.Options;

namespace GMS.NET.Core.Provider.CacheProvider
{
    /// <summary>
    /// 缓存选择器
    /// </summary>
    public class ICachingProvider : ISingleton
    {
        private readonly Func<string, ISingleton, object> Cachefunc = App.GetService<Func<string, ISingleton, object>>();
        /// <summary>
        /// 根据配置文件选择缓存
        /// </summary>
        /// <returns></returns>
        public ICachingService GetCache()
        {
            var CacheName = App.GetOptions<ApplicationOptions>().SystemCache!.Name;
            return (Cachefunc(CacheName!, default!) as ICachingService)!;
        }
       /// <summary>
       /// 根据枚举类型选择缓存
       /// </summary>
       /// <param name="cache"></param>
       /// <returns></returns>
        public ICachingService GetCache(CacheType cache)
        {
            var CacheName = cache.ToString();
            return (Cachefunc(CacheName!, default!) as ICachingService)!;
        }
    }
    /// <summary>
    /// 缓存类型
    /// </summary>
    public enum CacheType
    {
        /// <summary>
        /// mssql缓存
        /// </summary>
        SqlServer,
        /// <summary>
        /// redis
        /// </summary>
        Redis,
        /// <summary>
        /// net
        /// </summary>
        Memory
    }
}
