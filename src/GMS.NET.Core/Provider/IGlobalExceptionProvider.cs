﻿using Furion.DependencyInjection;
using Furion.EventBus;
using Furion.FriendlyException;
using GMS.NET.Const;
using GMS.NET.Core.Tools;
using Microsoft.AspNetCore.Mvc.Filters;

namespace GMS.NET.Core.Provider
{
    /// <summary>
    /// 全局异常捕获
    /// </summary>
    public class IGlobalExceptionProvider : IGlobalExceptionHandler, ISingleton
    {
        private readonly IEventPublisher events = App.GetService<IEventPublisher>();
        /// <summary>
        /// 异常捕获事件
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task OnExceptionAsync(ExceptionContext context)
        {
            await events.PublishAsync(EventBusConst.LogsWriteException,
                $"{UserInfo.UserId ?? ApiTool.GetIp()}{SplitCharConst.α}{context.Exception.Source}{SplitCharConst.α}{context.ActionDescriptor.DisplayName}{SplitCharConst.α}{context.Exception.Message}");
        }
    }
}
