﻿using Furion.Authorization;
using Furion.DataEncryption;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace GMS.NET.Web.Core.Security
{
    /// <summary>
    /// JWT身份认证
    /// </summary>
    public class JWTSafeHandler : AppAuthorizeHandler
    {
        /// <summary>
        /// 重写 Handler 添加自动刷新收取逻辑
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task HandleAsync(AuthorizationHandlerContext context)
        {
            var http = context.GetCurrentHttpContext();
            if (JWTEncryption.AutoRefreshToken(context, context.GetCurrentHttpContext()))
                await AuthorizeHandleAsync(context);
            else context.Fail();
        }
        /// <summary>
        /// 验证管道，也就是验证核心代码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public override async Task<bool> PipelineAsync(AuthorizationHandlerContext context, DefaultHttpContext httpContext) => await CheckAuthorzie(httpContext);
        /// <summary>
        /// 检查权限
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        private  Task<bool> CheckAuthorzie(DefaultHttpContext httpContext)
        {
            return Task.FromResult(true);

            // 获取权限特性
            //var SecurityDefine = httpContext.GetMetadata<SecurityDefineAttribute>();
            //if (SecurityDefine == null) return false;
            //else return true;
        }
    }
}
