namespace GMS.NET.SqlSugarCore.DbSeed.SeedDatas
{
	///<summary>
	///GMSNet_Customer DataSeed
	///</summary>
	public class GMSNet_Customer_DataSet : IDataSetRecovery<GMSNet_Customer>
	{
		public IEnumerable<GMSNet_Customer> HasData()
		{
			return new[]
			{
				new GMSNet_Customer()
				{
					Id=new Guid("4cfe4f86-c497-44bf-88d2-05feb7c62ca1"),
					Cust_Name="客户管理组",
					Cust_FullName="系统客户管理组",
					Cust_Connection="Server=127.0.0.1,1433;Database=GMSNet001;User Id=SA;Password=My_Str0ng_Password",
					Cust_Phone="001",
					Cust_Type=0,
					Cust_Address="",
					Cust_Status=1,
					Cust_Email="001@com",
					Creationtime=Convert.ToDateTime("2023/7/26 17:29:14"),
					Creator="GMSNetAdministrator",
					Modifytime=Convert.ToDateTime("2023/7/26 17:33:40"),
					Modifier="GMSNetAdministrator",
					Cust_Remark="",
					Cust_Deltime=null,
					Cust_Seq=1,
				},
				new GMSNet_Customer()
				{
					Id=new Guid("675ce1ee-b97e-4f3a-8b2d-3e214405c1a7"),
					Cust_Name="测试客户",
					Cust_FullName="测试客户组织",
					Cust_Connection="Server=127.0.0.1,1433;Database=GMSNet004;User Id=SA;Password=My_Str0ng_Password;TrustServerCertificate=true;",
					Cust_Phone="2",
					Cust_Type=0,
					Cust_Address="",
					Cust_Status=1,
					Cust_Email="2@.com",
					Creationtime=Convert.ToDateTime("2023/8/15 17:52:17"),
					Creator="wcy",
					Modifytime=Convert.ToDateTime("2023/8/15 17:52:17"),
					Modifier="wcy",
					Cust_Remark="",
					Cust_Deltime=null,
					Cust_Seq=4,
				},
			};
		}
	}
}
