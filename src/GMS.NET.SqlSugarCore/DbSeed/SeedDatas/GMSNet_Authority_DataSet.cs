namespace GMS.NET.SqlSugarCore.DbSeed.SeedDatas
{
	///<summary>
	///GMSNet_Authority DataSeed
	///</summary>
	public class GMSNet_Authority_DataSet : IDataSetRecovery<GMSNet_Authority>
	{
		public IEnumerable<GMSNet_Authority> HasData()
		{
			return new[]
			{
				new GMSNet_Authority()
				{
					Id=new Guid("7125e328-4fb3-458d-829b-271070076d2e"),
					Auth_RoleId=new Guid("9dfeba10-a410-4960-90de-faab2359f4dc"),
					Auth_ResId=new Guid("c6a38014-81dc-4b61-b562-6c89e1f35e56"),
					Auth_Remark="",
					Creationtime=Convert.ToDateTime("2023/8/25 14:52:30"),
					Creator="wcy",
					Modifytime=Convert.ToDateTime("2023/8/25 14:52:30"),
					Modifier="wcy",
				},
				new GMSNet_Authority()
				{
					Id=new Guid("bfdf1269-cd39-4b9d-b2f9-47227789b7bb"),
					Auth_RoleId=new Guid("9dfeba10-a410-4960-90de-faab2359f4dc"),
					Auth_ResId=new Guid("aa6c3d6d-e5cc-45e5-8307-eeaca7f26959"),
					Auth_Remark="",
					Creationtime=Convert.ToDateTime("2023/8/25 14:52:30"),
					Creator="wcy",
					Modifytime=Convert.ToDateTime("2023/8/25 14:52:30"),
					Modifier="wcy",
				},
				new GMSNet_Authority()
				{
					Id=new Guid("ba0cc9e6-e6d5-4478-b894-60f5312e4ec0"),
					Auth_RoleId=new Guid("9dfeba10-a410-4960-90de-faab2359f4dc"),
					Auth_ResId=new Guid("ef14c77e-04df-42e1-be26-b2f76ec119f6"),
					Auth_Remark="",
					Creationtime=Convert.ToDateTime("2023/8/25 14:52:30"),
					Creator="wcy",
					Modifytime=Convert.ToDateTime("2023/8/25 14:52:30"),
					Modifier="wcy",
				},
				new GMSNet_Authority()
				{
					Id=new Guid("847becb5-b45a-4ff0-b5a5-86e20b932207"),
					Auth_RoleId=new Guid("9dfeba10-a410-4960-90de-faab2359f4dc"),
					Auth_ResId=new Guid("3dc04126-6108-4cbe-9768-7c47adb43c8f"),
					Auth_Remark="",
					Creationtime=Convert.ToDateTime("2023/8/25 14:52:30"),
					Creator="wcy",
					Modifytime=Convert.ToDateTime("2023/8/25 14:52:30"),
					Modifier="wcy",
				},
				new GMSNet_Authority()
				{
					Id=new Guid("d4f1fea4-6c43-4ec2-8fe4-b06e2eb9b3a5"),
					Auth_RoleId=new Guid("9dfeba10-a410-4960-90de-faab2359f4dc"),
					Auth_ResId=new Guid("f23a51bf-e953-4e80-8f7a-3db4d07b0cdd"),
					Auth_Remark="",
					Creationtime=Convert.ToDateTime("2023/8/25 14:52:30"),
					Creator="wcy",
					Modifytime=Convert.ToDateTime("2023/8/25 14:52:30"),
					Modifier="wcy",
				},
				new GMSNet_Authority()
				{
					Id=new Guid("62e84e28-8b05-4856-8dad-f9250fc5a122"),
					Auth_RoleId=new Guid("9dfeba10-a410-4960-90de-faab2359f4dc"),
					Auth_ResId=new Guid("0f4b5e83-e41e-423b-8118-70acf80de87a"),
					Auth_Remark="",
					Creationtime=Convert.ToDateTime("2023/8/25 14:52:30"),
					Creator="wcy",
					Modifytime=Convert.ToDateTime("2023/8/25 14:52:30"),
					Modifier="wcy",
				},
			};
		}
	}
}
