namespace GMS.NET.SqlSugarCore.DbSeed.SeedDatas
{
	///<summary>
	///GMSNet_Role DataSeed
	///</summary>
	public class GMSNet_Role_DataSet : IDataSetRecovery<GMSNet_Role>
	{
		public IEnumerable<GMSNet_Role> HasData()
		{
			return new[]
			{
				new GMSNet_Role()
				{
					Id=new Guid("068b8b1e-8ce3-48d6-89aa-8b09e93be968"),
					Role_Name="系统管理员",
					Role_Status=1,
					Role_Remark="",
					Creationtime=Convert.ToDateTime("2023/7/26 17:29:41"),
					Creator="GMSNetAdministrator",
					Modifytime=Convert.ToDateTime("2023/7/26 17:29:41"),
					Modifier="GMSNetAdministrator",
					Role_Custs=new Guid("4cfe4f86-c497-44bf-88d2-05feb7c62ca1"),
				},
				new GMSNet_Role()
				{
					Id=new Guid("3ce006da-cf3b-47d7-8afe-d11c63ea54e0"),
					Role_Name="测试管理员的下级部门",
					Role_Status=1,
					Role_Remark="",
					Creationtime=Convert.ToDateTime("2023/8/16 16:45:35"),
					Creator="testLi",
					Modifytime=Convert.ToDateTime("2023/8/16 16:45:35"),
					Modifier="testLi",
					Role_Custs=new Guid("675ce1ee-b97e-4f3a-8b2d-3e214405c1a7"),
				},
				new GMSNet_Role()
				{
					Id=new Guid("9dfeba10-a410-4960-90de-faab2359f4dc"),
					Role_Name="测试管理员",
					Role_Status=1,
					Role_Remark="",
					Creationtime=Convert.ToDateTime("2023/8/15 18:14:00"),
					Creator="wcy",
					Modifytime=Convert.ToDateTime("2023/8/15 18:14:00"),
					Modifier="wcy",
					Role_Custs=new Guid("675ce1ee-b97e-4f3a-8b2d-3e214405c1a7"),
				},
			};
		}
	}
}
