namespace GMS.NET.SqlSugarCore.DbSeed.SeedDatas
{
	///<summary>
	///GMSNet_User DataSeed
	///</summary>
	public class GMSNet_User_DataSet : IDataSetRecovery<GMSNet_User>
	{
		public IEnumerable<GMSNet_User> HasData()
		{
			return new[]
			{
				new GMSNet_User()
				{
					Id=new Guid("4a2bec3c-5cbc-4c2a-af80-13d051ceadaf"),
					User_Account="testLi",
					User_PassWord="0527CC7FCCC7228C438DD803858913AB527FDE42D9011A1FC3306B90892C8065C66F350336300378D845D044A2D22579184B7EE1DF3176C1CB0291956424B1B2",
					User_Name=".Net开发工程师小李",
					User_CusId=new Guid("675ce1ee-b97e-4f3a-8b2d-3e214405c1a7"),
					User_Phone="06221063920",
					User_Email="0-Em0A6xLW@.com",
					User_RoleId=new Guid("9dfeba10-a410-4960-90de-faab2359f4dc"),
					User_Status=1,
					User_Type=1,
					Creationtime=Convert.ToDateTime("2023/8/15 18:10:14"),
					Creator="wcy",
					Modifytime=Convert.ToDateTime("2023/8/15 18:10:14"),
					Modifier="wcy",
					User_Remark="",
					User_OperCustsNotes=1,
				},
				new GMSNet_User()
				{
					Id=new Guid("3eef8665-e903-4119-bad8-e13710bd7072"),
					User_Account="wcy",
					User_PassWord="BA3253876AED6BC22D4A6FF53D8406C6AD864195ED144AB5C87621B6C233B548BAEAE6956DF346EC8C17F5EA10F35EE3CBC514797ED7DDD3145464E2A0BAB413",
					User_Name=".Net开发工程师小王",
					User_CusId=new Guid("4cfe4f86-c497-44bf-88d2-05feb7c62ca1"),
					User_Phone="",
					User_Email="1246736767@qq.com",
					User_RoleId=new Guid("068b8b1e-8ce3-48d6-89aa-8b09e93be968"),
					User_Status=1,
					User_Type=1,
					Creationtime=Convert.ToDateTime("2023/7/26 17:35:07"),
					Creator="GMSNetAdministrator",
					Modifytime=Convert.ToDateTime("2023/8/29 15:51:06"),
					Modifier="wcy",
					User_Remark="",
					User_OperCustsNotes=1,
				},
				new GMSNet_User()
				{
					Id=new Guid("349888d3-9667-4764-9673-fc55006ccdff"),
					User_Account="testZhang",
					User_PassWord="22B8576D45262BDE9AA3F7FC53A03D82758661CF05C629CBF483EA299D86B0BA8470BE4A6373A73C54745A6FC5E8F73A0F4ECA1484505EF2BEDD646D51C3200E",
					User_Name="小李的下级小张",
					User_CusId=new Guid("675ce1ee-b97e-4f3a-8b2d-3e214405c1a7"),
					User_Phone="07133984441",
					User_Email="0-lVLI09rP@.com",
					User_RoleId=new Guid("3ce006da-cf3b-47d7-8afe-d11c63ea54e0"),
					User_Status=1,
					User_Type=1,
					Creationtime=Convert.ToDateTime("2023/8/16 16:44:48"),
					Creator="testLi",
					Modifytime=Convert.ToDateTime("2023/8/16 16:48:22"),
					Modifier="testLi",
					User_Remark="",
					User_OperCustsNotes=0,
				},
			};
		}
	}
}
