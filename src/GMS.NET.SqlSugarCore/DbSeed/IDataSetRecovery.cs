﻿namespace GMS.NET.SqlSugarCore.DbSeed
{
    public interface IDataSetRecovery<TEntity> where TEntity : class, new()
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> HasData();
    }
}
