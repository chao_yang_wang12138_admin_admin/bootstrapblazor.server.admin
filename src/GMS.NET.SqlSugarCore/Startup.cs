﻿using Furion;
using GMS.NET.Core.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using SqlSugar;

namespace GMS.NET.SqlSugarCore
{
    public class Startup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddConfigurableOptions<ConnectionStringsOptions>();
            services.AddSingleton<ISqlSugarClient>(s =>
            {
                SqlSugarScope sqlSugar = new SqlSugarScope(new ConnectionConfig()
                {
                    DbType = SqlSugar.DbType.SqlServer,
                    ConnectionString = App.GetOptions<ConnectionStringsOptions>().MasterDataBank,
                    IsAutoCloseConnection = true,
                    ConfigId = App.GetOptions<ConnectionStringsOptions>().ConfigId
                },
               db =>
               {
                   //单例参数配置，所有上下文生效
                   db.Aop.OnLogExecuting = (sql, pars) =>
                   {
                       //获取作IOC作用域对象
                       //var appServive = s.GetService<IHttpContextAccessor>();
                       //var obj = appServive?.HttpContext?.RequestServices.GetService<Log>();
                       //Console.WriteLine("AOP" + obj.GetHashCode());
                   };
               });
                return sqlSugar;
            });
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

        }
    }
}