﻿using Furion.DependencyInjection;
using Furion.EventBus;
using GMS.NET.Const;
using GMS.NET.Core.Entitys.System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace GMS.NET.Application.EventBus
{
    /// <summary>
    /// 操作日志事件管理
    /// </summary>
    public class LogsEventSubscriber : DbManger, IEventSubscriber, ISingleton
    {
        private readonly ILogger<LogsEventSubscriber> ILog;
        public LogsEventSubscriber( ILogger<LogsEventSubscriber> logger)
        {
            ILog = logger;
        }

        [EventSubscribe(EventBusConst.LogsWriteBasic)]
        public async Task LogsWriteBasic(EventHandlerExecutingContext context)
        {
            var log = context.Source.Payload as GMSNet_LOG;
            await MasterDb.Insertable(log).SplitTable().ExecuteCommandAsync();
        }
        [EventSubscribe(EventBusConst.LogsWriteApi)]
        public  Task LogsWriteApi(EventHandlerExecutingContext context)
        {
            var logMessage = context.Source.Payload as string;
            ILog.LogInformation(logMessage);
            return Task.CompletedTask;
        }
        [EventSubscribe(EventBusConst.LogsWriteException)]
        public Task LogsWriteException(EventHandlerExecutingContext context)
        {
            var logMessage = context.Source.Payload as string;
            ILog.LogError(logMessage);
            return Task.CompletedTask;
        }
    }
}
