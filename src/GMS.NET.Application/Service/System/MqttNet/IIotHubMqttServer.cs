﻿using MQTTnet.Server;

namespace GMS.NET.Application.Service.System.MqttNet
{
    public interface IIotHubMqttServer
    {
        public MqttServer? MqttServer { get; }


        void ConfigureMqttServer(MqttServer mqttServer);
    }
}
