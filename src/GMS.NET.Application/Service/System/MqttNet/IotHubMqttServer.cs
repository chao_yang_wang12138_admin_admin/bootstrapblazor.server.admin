﻿using MQTTnet.Server;

namespace GMS.NET.Application.Service.System.MqttNet
{
    public class IotHubMqttServer : IIotHubMqttServer
    {
        public MqttServer? MqttServer { get; private set; }

        private readonly IMqttServerService _mqttServerService;

        public IotHubMqttServer(IMqttServerService mqttServerService)
        {
            _mqttServerService = mqttServerService;
        }

        public void ConfigureMqttServer(MqttServer mqttServer)
        {
            MqttServer = mqttServer;
            _mqttServerService.ConfigureMqttService(mqttServer);
        }
    }
}
