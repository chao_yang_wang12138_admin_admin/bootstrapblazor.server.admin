﻿using MQTTnet.Server;

namespace GMS.NET.Application.Service.System.MqttNet
{
    public interface IMqttServerService
    {
        public MqttServer? MqttServer { get; }



        void ConfigureMqttService(MqttServer mqttServer);
    }
}
