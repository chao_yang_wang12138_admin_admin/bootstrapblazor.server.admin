﻿using MQTTnet.Server;

namespace GMS.NET.Application.Service.System.MqttNet
{
    public class MqttServerBase : IMqttServiceBase
    {
        public MqttServer MqttServer { get; private set; }

        public MqttServerBase()
        {

        }

        public virtual void ConfigureMqttServer(MqttServer mqttServer)
        {
            this.MqttServer = mqttServer;

        }


        protected virtual async Task<MqttClientStatus?> GetClientStatusAsync(string clientId)
        {
            var allClientStatus = await MqttServer.GetClientsAsync();
            return allClientStatus.FirstOrDefault(x => x.Id == clientId);
        }
    }
}
