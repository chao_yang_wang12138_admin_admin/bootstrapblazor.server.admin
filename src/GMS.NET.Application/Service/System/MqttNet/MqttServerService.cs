﻿using MQTTnet.Server;

namespace GMS.NET.Application.Service.System.MqttNet
{
    public class MqttServerService : IMqttServerService
    {
        public MqttServer? MqttServer { get; private set; }
        private readonly IMqttConnectionService _mqttConnectionService;
        private readonly IMqttSubscriptionService _mqttSubscriptionService;

        public MqttServerService(IMqttConnectionService mqttConnectionService,
            IMqttSubscriptionService mqttSubscriptionService)
        {
            _mqttConnectionService = mqttConnectionService;
            _mqttSubscriptionService = mqttSubscriptionService;
        }


        public void ConfigureMqttService(MqttServer mqttServer)
        {
            MqttServer = mqttServer;

            _mqttConnectionService.ConfigureMqttServer(mqttServer);
            _mqttSubscriptionService.ConfigureMqttServer(mqttServer);
        }
    }
}
