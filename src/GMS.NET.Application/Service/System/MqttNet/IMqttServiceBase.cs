﻿using MQTTnet.Server;

namespace GMS.NET.Application.Service.System.MqttNet
{
    public interface IMqttServiceBase
    {
        public MqttServer MqttServer { get; }

        /// <summary>
        /// 配置MqttServer
        /// </summary>
        /// <param name="mqttServer"></param>
        void ConfigureMqttServer(MqttServer mqttServer);
    }
}