﻿namespace GMS.NET.Application.Service.System.Topics
{
    public static class BrokerTopics
    {
        public const string SysTopisPrefix = "/sys";

        public static class Event
        {
            public const string Base = $"{SysTopisPrefix}/broker";
            public const string ClientBase = $"{Base}/clients";

            public const string SubscriptionsCount = $"{Base}/subscriptions/count";

            public const string ConnectedClients = $"{ClientBase}/connected";
            public const string DisconnectedClients = $"{ClientBase}/disconnected";
            public const string NewClientConnected = $"{ClientBase}/connected/new";
            public const string NewClientDisconnected = $"{ClientBase}/disconnected/new";
            public const string ConnectedClientsCount = $"{ClientBase}/connected/count";
            public const string DisconnectedClientsCount = $"{ClientBase}/disconnected/count";
            public static string ClientIp(string clientId) => $"{ClientBase}/{clientId}/ip";
            public static string ClientConnectedTime(string clientId) => $"{ClientBase}/{clientId}/connectedtime";

            //消息 
            public const string MessageBase = $"dk/receiveMessage"; 
        }

        public static class Command
        {
            public const string Base = $"{SysTopisPrefix}/request/broker";
            public const string ClientBase = $"{Base}/clients";

            public const string GetSubscriptionCount = $"{Base}/subscriptions/count";

            public const string GetConnectedClients = $"{ClientBase}/connected";
            public const string GetDisconnectedClients = $"{ClientBase}/disconnected";
            public const string GetConnectedClientsCount = $"{ClientBase}/connected/count";
            public const string GetDisconnectedClientsCount = $"{ClientBase}/disconnected/count";
            public const string GetClientIp = $"{ClientBase}/ip";
            public const string GetClientConnectedTime = $"{ClientBase}/connectedtime";
            public const string PatchDisconnectClient = $"{ClientBase}/disconnect/command";
        }
         

        public static class Secret
        {
            public const string Base = $"{SysTopisPrefix}/secret/broker";
        }
    }
}
