﻿
namespace GMS.NET.Application.Service.System.Auth
{
    public class AuthUser
    {
        public string? User_Account { get; set; }
        public int User_Status { get; set; }
        public int User_OperCustsNotes { get; set; }
        public string? User_Name { get; set; }
        public int User_Type { get; set; }
        public Guid User_RoleId { get; set; }
        public Guid User_CusId { get; set; }
        public string? Cust_Name { get; set; }
        public int Cust_Status { get; set; }
        public int Cust_Type { get; set; }
        public int Role_Status { get; set; }
        public string? Cust_Connection { get; set; }
        public bool IsAuth { get; set; }
    }
}
