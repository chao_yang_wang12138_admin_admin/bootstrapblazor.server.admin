﻿using GMS.NET.Dto.System.Auth.Shared;

namespace GMS.NET.Application.Service.Auth
{
    public interface IAuthService
    {
        /// <summary>
        /// 授权登陆
        /// </summary>
        /// <returns></returns>
        Task LoginAuth([FromBody] LoginAuth loginAuth);
        /// <summary>
        /// 发送邮件验证码
        /// </summary>
        /// <param name="EmailCode"></param>
        /// <returns></returns>
        Task SendEmailCode([FromBody] LoginAuth loginAuth);
        /// <summary>
        /// 发送手机验证码
        /// </summary>
        /// <param name="PhoneCode"></param>
        /// <returns></returns>
        Task SendPhoneCode([FromBody] LoginAuth loginAuth);
    }
}
