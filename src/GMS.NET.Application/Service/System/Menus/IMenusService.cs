using GMS.NET.Dto.System.Menus.Shared;

namespace GMS.NET.Application.Service.System.Menus
{
    public interface IMenusService
    {
        /// <summary>
        /// 获取代码生成信息
        /// </summary>
        /// <returns></returns>
        Task<CodeCSInfo> GetCodeInfo(Guid Id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="status"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task MenusTreeStatus(int status, Guid Id);
        /// <summary>
        /// 获取系统菜单树
        /// </summary>
        /// <returns></returns>
        Task<List<MenusTree>> GetMenusTree([FromBody] Guid ParentId);
        /// <summary>
        /// 保存系统菜单树
        /// </summary>
        /// <returns></returns>
        Task SaveMenusTree([FromBody] MenusTree menusTree);
        /// <summary>
        ///  删除系统菜单树
        /// </summary>
        /// <returns></returns>
        Task DelMenusTree([FromBody] List<Guid> Ids);

        /// <summary>
        /// 获取父级列表
        /// </summary>
        /// <returns></returns>
        Task<List<KeyValues>> GetParentList([FromBody] string Text);
    }
}