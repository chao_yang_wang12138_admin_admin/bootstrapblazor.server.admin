﻿using GMS.NET.Application.Service.System.Topics;
using GMS.NET.Consts;

namespace GMS.NET.Application.Service.System.Extensions
{
    public static class MqttTopicHelper
    {
        public static bool IsSystemTopic(string topic)
        {
            return topic.StartsWith(BrokerTopics.SysTopisPrefix, StringComparison.OrdinalIgnoreCase);
        }


        public static bool IsBrokerItself(string clientId)
        {
            return clientId == MqttNetConst.IotHubMqttClientId;
        }
    }
}
