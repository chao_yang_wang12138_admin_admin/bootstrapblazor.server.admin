﻿using GMS.NET.Core.Entitys.Business;
using GMS.NET.Dto.System.Test.Output;
using GMS.NET.SqlSugarCore.DbCore;

namespace GMS.NET.Application.Service.System.Test
{
    [Authorize]
    [DynamicApiController]
    public class TestService : DbManger, ITestService
    {
        [HttpPost]
        public async Task<TabularData<TestData>> GetTestData([FromBody] QueryPivot query)
        {
            RefAsync<int> total = 0;
            var result = await BizDb.Queryable<TestTable>().Select<TestData>()
                .Where(query.WhereModel!.GetSearches())
                .ToPageListAsync(query.PageIndex, query.PageSize,total);
            return new TabularData<TestData>() { Items = result,page_Total=total };
        }
    }
}
