﻿using GMS.NET.Dto.System.Test.Output;

namespace GMS.NET.Application.Service.System.Test
{
    public interface ITestService
    {
        Task<TabularData<TestData>> GetTestData([FromBody] QueryPivot query);
    }
}
