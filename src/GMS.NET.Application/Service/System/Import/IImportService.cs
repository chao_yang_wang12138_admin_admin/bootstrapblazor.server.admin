﻿using GMS.NET.Dto.Basic;

namespace GMS.NET.Application.Service.System.Import
{
    public interface IImportService
    {
        /// <summary>
        /// 上传excel
        /// </summary>
        /// <returns></returns>
        Task<List<string>> UploadExcelData();
        /// <summary>
        /// 获取上传的excel数据
        /// </summary>
        /// <returns></returns>
        Task<TabularData<Dictionary<string,string>>> GetExcelData([FromBody] QueryPivot queryPivot);
        /// <summary>
        /// 获取检验后的excel结果
        /// </summary>
        /// <returns></returns>
        Task<MemoryStream> GetExcelCheckData();
    }
}
