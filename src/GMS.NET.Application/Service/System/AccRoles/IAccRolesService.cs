﻿using GMS.NET.Dto.System.AccRoles.Input;
using GMS.NET.Dto.System.AccRoles.Output;
using GMS.NET.Dto.System.AccRoles.Shared;

namespace GMS.NET.Application.Service.System.Roles
{
    public interface IAccRolesService
    {
        Task RoleStatus(int status, Guid Id);
        /// <summary>
        /// 检测角色名称是否重复
        /// </summary>
        /// <param name="roleNameText"></param>
        /// <returns></returns>
        Task<bool> CheckRoleNameRepeat([FromBody] string roleName);
        /// <summary>
        /// 获取角色信息
        /// </summary>
        /// <param name="queryPivot"></param>
        /// <returns></returns>
        Task<TabularData<RolesData>> GetRolesData([FromBody] QueryPivot queryPivot);
        /// <summary>
        /// 保存角色信息
        /// </summary>
        /// <param name="rolesData"></param>
        /// <returns></returns>
        Task SaveRolesData([FromBody] RolesData rolesData);
        /// <summary>
        /// 删除角色信息
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        Task DelRolesData([FromBody] List<Guid> Ids);
        /// <summary>
        /// 获取该角色下可选用户
        /// </summary>
        /// <returns></returns>
        Task<TabularData<WaitUsers>> GetWaitUsers([FromBody] QueryPivot queryPivot);
        /// <summary>
        /// 获取该角色下可选资源
        /// </summary>
        /// <returns></returns>
        Task<List<WaitRess>> GetWaitRess([FromBody] Guid RoleId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="acc"></param>
        /// <returns></returns>
        Task SetAccUsers([FromBody] AccUsers acc);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="acc"></param>
        /// <returns></returns>
        Task SetAccRess([FromBody] AccRess acc);
    }
}
