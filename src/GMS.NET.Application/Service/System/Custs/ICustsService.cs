﻿using GMS.NET.Dto.System.Custs.Shared;

namespace GMS.NET.Application.Service.System.Custs
{
    public interface ICustsService
    {
        Task CustsStatus(int status, Guid Id);
        /// <summary>
        /// 检测用户时候已经了解注册客户注意事项
        /// </summary>
        /// <returns></returns>
        Task<bool> CheckUsersConfirmOperCustsNotes([FromBody] bool IsCheck);
        /// <summary>
        /// 获取客户信息
        /// </summary>
        /// <param name="queryPivot"></param>
        /// <returns></returns>
        Task<TabularData<CustsData>> GetCustsData([FromBody] QueryPivot queryPivot);
        /// <summary>
        /// 保存客户信息
        /// </summary>
        /// <param name="custsData"></param>
        /// <returns></returns>
        Task SaveCustsData([FromBody] CustsData custsData);
        /// <summary>
        /// 删除客户信息
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        Task DelCustsData([FromBody] List<Guid> Ids);
        /// <summary>
        /// 恢复删除客户信息
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        Task RecoverCustsData([FromBody] List<Guid> Ids);
    }
}
