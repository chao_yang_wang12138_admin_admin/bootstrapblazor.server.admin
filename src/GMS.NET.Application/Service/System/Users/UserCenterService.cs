﻿
using GMS.NET.Const;
using GMS.NET.Core.Entitys.System;
using GMS.NET.Dto.System.Users.UserCenter.Shared;
using GMS.NET.Encrypt;
using System.Security.Cryptography;
using RSA = GMS.NET.Encrypt.RSA;

namespace GMS.NET.Application.Service.System.Users
{
    [Authorize]
    [DynamicApiController]
    public class UserCenterService :DbManger, IUserCenterService
    {
        public async Task<bool> CheckOldPas([FromBody] string Coding)
        {
            var privateKey = App.Configuration.GetSection(EncryptConst.PrivateKey).Value ?? "";
            var oldPasstr = RSA.Decrypt(Coding, privateKey);
            var dbPassstr = SHA.Hash512(oldPasstr);
            return await MasterDb.Queryable<GMSNet_User>()
                .AnyAsync(X => X.User_Account == UserInfo.UserId &&
                X.User_PassWord == dbPassstr);
        }

        [HttpPost]
        public async Task<UserInfoCenter> GetUserInfo()
        {
            return await MasterDb.Queryable<GMSNet_User>()
                .Where(x => x.User_Account == UserInfo.UserId)
                .Select<UserInfoCenter>()
                .FirstAsync();
        }

        public async Task UpdUserPas([FromBody] string Coding)
        {
            //获取私钥
            var privateKey = App.Configuration.GetSection(EncryptConst.PrivateKey).Value ?? "";
            var newPasstr = RSA.Decrypt(Coding, privateKey);
            var dbPassstr = SHA.Hash512(newPasstr);
            await MasterDb.Updateable<GMSNet_User>()
                .SetColumns(x => x.User_PassWord == dbPassstr)
                .Where(x => x.User_Account == UserInfo.UserId)
                .ExecuteCommandAsync();
        }
    }
}
