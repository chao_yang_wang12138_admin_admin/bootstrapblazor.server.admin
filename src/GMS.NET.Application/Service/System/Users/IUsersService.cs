﻿using GMS.NET.Dto.System.Users.Shared;

namespace GMS.NET.Application.Service.System.Users
{
    public interface IUsersService
    {
        Task UserStatus(int status, Guid Id);
        /// <summary>
        /// 获取用户excel模板
        /// </summary>
        /// <returns></returns>
        Task<MemoryStream> GetExcelTemp();
        /// <summary>
        /// 用户数据检测
        /// </summary>
        /// <returns></returns>
        Task UsersDataCheck();
        /// <summary>
        /// 用户数据导入
        /// </summary>
        /// <returns></returns>
        Task UsersDataImport();
        /// <summary>
        /// 获取用户数据信息
        /// </summary>
        /// <param name="queryPivot"></param>
        /// <returns></returns>
        Task<TabularData<UsersData>> GetUsers([FromBody] QueryPivot queryPivot);
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        Task DelUser([FromBody] List<Guid> Ids);
        /// <summary>
        /// 保存用户信息
        /// </summary>
        /// <param name="usersData"></param>
        /// <returns></returns>
        Task<string> SaveUser([FromBody] UsersData usersData);
        /// <summary>
        ///  获取角色下拉
        /// </summary>
        /// <param name="Text"></param>
        /// <returns></returns>
        Task<List<KeyValues>> GetRolesList([FromBody] string Text);
        /// <summary>
        ///  获取客户下拉
        /// </summary>
        /// <returns></returns>
        Task<List<KeyValues>> GetCustsList([FromBody] string Text);
    }
}
