﻿
using GMS.NET.Dto.System.Users.UserCenter.Shared;

namespace GMS.NET.Application.Service.System.Users
{
    public interface IUserCenterService
    {
        Task<UserInfoCenter> GetUserInfo();
        Task<bool> CheckOldPas([FromBody] string Coding);
        Task UpdUserPas([FromBody] string Coding);
    }
}
