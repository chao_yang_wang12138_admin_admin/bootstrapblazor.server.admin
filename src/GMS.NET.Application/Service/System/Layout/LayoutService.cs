﻿using GMS.NET.Const;
using GMS.NET.Core.Entitys.System;
using GMS.NET.Dto.System.Layout.Ouput;
using GMS.NET.Dto.System.Menus.Enums;
using GMS.NET.SqlSugarCore.DbCore;
using Mapster;

namespace GMS.NET.Application.Service.System.Layout
{
    [Authorize]
    [DynamicApiController]
    public class LayoutService : DbManger, ILayoutService
    {
        [HttpPost]
        public async Task<List<MenuItemSync>> GetMenus()
        {
            var menuItems = await MasterDb.Queryable<GMSNet_Resouce>()
                .InnerJoinIF<GMSNet_Authority>(!UserInfo.UserAdmin, (x, y) => x.Id == y.Auth_ResId)
                .WhereIF(!UserInfo.UserAdmin, (x, y) => y.Auth_RoleId == Guid.Parse(UserInfo.RoleId)
                && x.Res_Disabled==(int)Res_Disabled.normal )
              .Where(x => x.Res_Type != (int)Res_Type.apiInterface &&
              x.Res_Type != (int)Res_Type.field )
              .OrderBy(x => x.Res_Sort, OrderByType.Asc)
              .ToTreeAsync(it => it.Children, it => it.Res_ParentId, DataConst.TopParentGuid);
            var result = menuItems.Adapt<List<MenuItemSync>>();
            return result;
        }
        public async Task<bool> IsIndex()
        {
            return await MasterDb.Queryable<GMSNet_Resouce>()
                .InnerJoinIF<GMSNet_Authority>(!UserInfo.UserAdmin, (x, y) => x.Id == y.Auth_ResId)
                .WhereIF(!UserInfo.UserAdmin, (x, y) => y.Auth_RoleId == Guid.Parse(UserInfo.RoleId) && x.Res_Url == "/")
                .AnyAsync();
        }
    }
}
