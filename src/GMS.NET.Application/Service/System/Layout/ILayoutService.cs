﻿using GMS.NET.Dto.System.Layout.Ouput;

namespace GMS.NET.Application.Service.System.Layout
{
    /// <summary>
    /// 布局页面服务
    /// </summary>
    public interface ILayoutService
    {
        /// <summary>
        /// 获取系统布局菜单列表
        /// </summary>
        /// <returns></returns>
        Task<List<MenuItemSync>> GetMenus();
        Task<bool> IsIndex();
    }
}
