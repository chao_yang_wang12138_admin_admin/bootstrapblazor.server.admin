namespace GMS.NET.Application.Service.System.CodeCreate
{
	/// <summary>
	/// 创建页面
	/// </summary>
	public interface ICodeCreateService
	{
		/// <summary>
		/// 创建页面api
		/// </summary>
		/// <returns></returns>
		Task TestApi();
		
	}
}
