namespace GMS.NET.Application.Service.System.CodeCreate
{
	/// <summary>
	/// 创建页面
	/// </summary>
	[Authorize]
	[DynamicApiController]
	public class CodeCreateService :  DbManger, ICodeCreateService
	{
		/// <summary>
		/// 创建页面api
		/// </summary>
		/// <returns></returns>
		[SecurityDefine("c79b4149-322d-4c20-95f6-a1076e966455")]
		public Task TestApi()=>Task.CompletedTask;
		
	}
}
