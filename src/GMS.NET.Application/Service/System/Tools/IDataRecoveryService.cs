﻿namespace GMS.NET.Application.Service.System.Tools
{
    /// <summary>
    /// 数据恢复工具
    /// </summary>
    public interface IDataRecoveryService
    {
        /// <summary>
        /// 全部数据恢复
        /// </summary>
        /// <returns></returns>
        Task FullRecovery();
        /// <summary>
        /// 指定实体类数据恢复
        /// </summary>
        /// <param name="EntityName">实体类名称</param>
        /// <returns></returns>
        Task AssignRecovery(string EntityName);
    }
}
