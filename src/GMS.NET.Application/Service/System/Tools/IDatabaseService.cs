﻿
namespace GMS.NET.Application.Service.System.Tools
{
    /// <summary>
    /// 数据表管理工具
    /// </summary>
    public interface IDatabaseService
    {
        /// <summary>
        /// 根据命名空间获取实体并创建数据库表
        /// </summary>
        /// <param name="Param"></param>
        /// <returns></returns>
        void CreateEntitysToDatabaseTable([FromBody] string nameSpace);
        /// <summary>
        /// 获取excel创表数据库模板文件流
        /// </summary>
        /// <returns></returns>
        Task<MemoryStream> DatabaseExcelTempStream();
        /// <summary>
        /// 根据excel创建excel实体和数据库表
        /// </summary>
        /// <returns></returns>
        Task CreateExcelToEntitysFileAndDbTable([FromQuery] string nameSpace);
        /// 数据恢复工具
        
    }
}
