﻿using GMS.NET.Dto.System.Tools.Input;

namespace GMS.NET.Application.Service.System.Tools
{
    public interface IDataSeedService
    {
        /// <summary>
        /// 根据命名空间获取数据并创建相应的实体种子
        /// </summary>
        /// <param name="Param"></param>
        /// <returns></returns>
        void CreateDataBaseToDataSeedEntitys([FromBody] DataSeedDescr nameSpace);
    }
}
