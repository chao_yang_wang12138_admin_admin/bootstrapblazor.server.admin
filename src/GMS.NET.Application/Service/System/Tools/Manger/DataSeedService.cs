﻿using GMS.NET.Dto.System.Tools.Input;
using GMS.NET.SqlSugarCore.DbCore;
using NetTaste;
using System.Text;

namespace GMS.NET.Application.Service.System.Tools.Manger
{
    [AllowAnonymous]
    [DynamicApiController]
    public class DataSeedService : DbManger, IDataSeedService
    {
        public async void CreateDataBaseToDataSeedEntitys([FromBody] DataSeedDescr nameSpace)
        {
            var EntitySeedUrl = App.Configuration.GetSection("EntitySeedFileDir").Value;
            // 获取命名空间下的表
            var tables = App.EffectiveTypes.Where(t => t.Namespace == nameSpace.EntityNameSpace && t.IsClass)
                .Select(x => x.Name).ToList();
            var _tables = MasterDb.DbMaintenance.GetTableInfoList(false);
            foreach (var table in tables)
            {
                if (_tables.Any(x => x.Name == table) && !table.Contains("LOG"))
                {
                    var EntityFileUrl = $"{EntitySeedUrl}{table}_DataSet.txt";
                    var cols = MasterDb.DbMaintenance.GetColumnInfosByTableName(table);
                    var TableData = await MasterDb.Queryable<object>().AS(table, "s").ToDictionaryListAsync();
                    if (TableData.Count() > 0)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine($"namespace {nameSpace.DataSeedEntityNameSpace}");
                        sb.AppendLine("{");
                        sb.AppendLine("\t///<summary>");
                        sb.AppendLine($"\t///{table} DataSeed");
                        sb.AppendLine("\t///</summary>");
                        sb.AppendLine($"\tpublic class {table}_DataSet : IDataSetRecovery<{table}>");
                        sb.AppendLine("\t{");
                        sb.AppendLine($"\t\tpublic IEnumerable<{table}> HasData()");
                        sb.AppendLine("\t\t{");
                        sb.AppendLine("\t\t\treturn new[]");
                        sb.AppendLine("\t\t\t{");
                        foreach (var item in TableData)
                        {
                            sb.AppendLine($"\t\t\t\tnew {table}()");
                            sb.AppendLine("\t\t\t\t{");
                            foreach (var it in item)
                            {
                                switch (cols.First(x=>x.DbColumnName==it.Key).DataType)
                                {
                                    case "uniqueidentifier":
                                        sb.AppendLine($"\t\t\t\t\t{it.Key}=new Guid({"\"" + it.Value + "\""}),");
                                        break;
                                    case "int":
                                        sb.AppendLine($"\t\t\t\t\t{it.Key}={it.Value??0},");
                                        break;
                                    case "varchar":
                                        sb.AppendLine($"\t\t\t\t\t{it.Key}={"\""+it.Value+"\""},");
                                        break;
                                    case "datetime":
                                        sb.AppendLine($"\t\t\t\t\t{it.Key}={(it.Value is null?"null":$"Convert.ToDateTime({"\"" + it.Value + "\""})")},");
                                        break;
                                    default:
                                        break;
                                }
                            }
                            sb.AppendLine("\t\t\t\t},");
                        }
                        sb.AppendLine("\t\t\t};");
                        sb.AppendLine("\t\t}");
                        sb.AppendLine("\t}");
                        sb.AppendLine("}");
                        File.WriteAllText(EntityFileUrl, sb.ToString());
                        string newFilePath = Path.ChangeExtension(EntityFileUrl, ".cs");
                        File.Move(EntityFileUrl, newFilePath, true);
                    }
                }
            }
        }
    }
}
