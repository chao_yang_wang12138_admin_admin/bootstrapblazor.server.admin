﻿using GMS.NET.Core.Entitys.Excel.Basic;
using GMS.NET.Core.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace GMS.NET.Application;

/// <summary>
/// 
/// </summary>
public class Startup : AppStartup
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="services"></param>
    public void ConfigureServices(IServiceCollection services)
    {
        //MqNet
        //services.AddIotHubMqttServer(configure =>{ });
        // excel服务类
        services.AddTransient<IExcelService, ExcelService>();
        var CacheOptions = App.GetOptions<ApplicationOptions>().SystemCache;
        switch (CacheOptions!.Name)
        {
            case "Redis":
                //Microsoft.Extensions.Caching.StackExchangeRedis
                services.AddStackExchangeRedisCache(options =>
                {
                    options.Configuration = CacheOptions.ConnectString;
                    options.InstanceName = "gmsnet_";
                });
                break;
            case "SqlServer":
                //需要在数据库创建缓存表 可使用以下命令创建
                //var install = "dotnet tool install --global dotnet-sql-cache --version 6.0.16";
                //var cachetb = $"dotnet sql-cache create '{CacheOptions.ConnectString}' dbo GMSNet_Cache";
                //CmdTool.Start(install);
                //CmdTool.Start(cachetb);
                //Microsoft.Extensions.Caching.SqlServer
                services.AddDistributedSqlServerCache(options =>
                {
                    options.ConnectionString = CacheOptions.ConnectString;
                    options.SchemaName = "dbo";
                    options.TableName = "GMSNet_Cache";
                });
                break;
            default: break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="app"></param>
    /// <param name="env"></param>
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        //app.UseIotHubMqttServer();
    }
}