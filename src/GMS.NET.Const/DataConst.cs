﻿namespace GMS.NET.Const
{
    public class DataConst
    {
        /// <summary>
        /// 父级id
        /// </summary>
        public const string TopParentGuid = "00000000-0000-0000-0000-000000000000";
        /// <summary>
        /// 未设置排序条件
        /// </summary>
        public const string UnSort = "UnSet";
    }
}
