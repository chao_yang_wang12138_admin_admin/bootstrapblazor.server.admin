﻿namespace GMS.NET.Const
{
    public class EntityfieldConst
    {
        /// <summary>
        /// 创建时间
        /// </summary>
        public const string Creationtime = "Creationtime";
        /// <summary>
        /// 修改时间
        /// </summary>
        public const string Modifytime = "Modifytime";
    }
}
