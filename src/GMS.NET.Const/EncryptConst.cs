﻿namespace GMS.NET.Const
{
    public class EncryptConst
    {
        public const string PublicKey = "PublicKey";
        public const string PrivateKey = "PrivateKey";
    }
}
