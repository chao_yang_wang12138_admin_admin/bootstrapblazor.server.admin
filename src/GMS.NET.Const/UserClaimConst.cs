﻿namespace GMS.NET.Const
{
    public class UserClaimConst
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public const string UserId = "UserId";
        /// <summary>
        /// 用户账号
        /// </summary>
        public const string UserName = "UserName";
        /// <summary>
        /// 用户账号
        /// </summary>
        public const string UserAdmin = "UserAdmin";
        /// <summary>
        /// 所属组织Id
        /// </summary>
        public const string CusId = "CusId";
        /// <summary>
        /// 所属角色Id
        /// </summary>
        public const string RoleId = "RoleId";
        /// <summary>
        /// 所属组织名称
        /// </summary>
        public const string CusName = "CusName";
        /// <summary>
        /// 用户令牌
        /// </summary>
        public const string AccessToken = "AccessToken";
        /// <summary>
        /// 缓存名称后缀
        /// 数据库连接字符串
        /// <param name="userId"></param>
        /// </summary>
        public static string CacheConnectString(string userId) => $"{userId}ConnectString";
        /// <summary>
        /// 缓存名称后缀
        /// excel数据存储
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string CacheExcelData(string userId) => $"{userId}CacheExcelData";
    }
}
