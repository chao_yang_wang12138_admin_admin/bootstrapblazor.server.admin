﻿namespace GMS.NET.Const
{
    public class ApiHeaderConst
    {
        /// <summary>
        /// 时间戳
        /// </summary>
        public const string Timestamp = "Timestamp";
        /// <summary>
        /// 签名
        /// </summary>
        public const string Signature = "Signature";
        /// <summary>
        /// 服务器验证标记
        /// </summary>
        public const string ServiceValidationTag = "ServiceValidationTag";
        /// <summary>
        /// 授权失败
        /// </summary>
        public const string Unauthorized401 = "401 Unauthorized";
        /// <summary>
        /// 服务器拒绝访问
        /// </summary>
        public const string Forbidden403 = "403 Forbidden";
    }
}
