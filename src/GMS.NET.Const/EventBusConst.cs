﻿namespace GMS.NET.Const
{
    public class EventBusConst
    {
        /// <summary>
        /// 添加客户初始化数据表
        /// </summary>
        public const string CustsAddInitializeDataBase = "CustsAddInitializeDataBase";
        /// <summary>
        ///  日志基础写入
        /// </summary>
        public const string LogsWriteBasic = "LogsWriteBasic";
        /// <summary>
        ///  日志api写入接口
        /// </summary>
        public const string LogsWriteApi = "LogsWriteApi";
        /// <summary>
        ///  日志异常写入异常
        /// </summary>
        public const string LogsWriteException = "LogsWriteException";
    }
}
