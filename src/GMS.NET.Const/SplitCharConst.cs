﻿
namespace GMS.NET.Const
{
    public class SplitCharConst
    {
        public const string ß = "ß";
        public const string þ = "þ";
        public const string α = "α";
    }
}
