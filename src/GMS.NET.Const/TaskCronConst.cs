﻿namespace GMS.NET.Const
{
    public class TaskCronConst
    {
        /// <summary>
        /// 每天一次 中午12点
        /// </summary>
        public const string EveryOne12 = "0 0 12 * * ?";
        /// <summary>
        /// 每三秒执行一次
        /// </summary>
        public const string Every3s = "0/3 * * * * ?";
    }
}