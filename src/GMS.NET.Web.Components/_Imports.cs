﻿global using GMS.NET.Web.Components.Data;
global using WebApiClientCore;
global using WebApiClientCore.Attributes;
global using GMS.NET.Web.Components.Shared.Global.Identity;
global using GMS.NET.Web.Components.Shared.Global.Attributes;
global using BootstrapBlazor.Components;
global using GMS.NET.Const;
global using GMS.NET.Dto.Basic;