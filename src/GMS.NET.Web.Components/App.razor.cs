﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using System.Diagnostics.CodeAnalysis;

namespace GMS.NET.Web.Components
{
    /// <summary>
    /// 
    /// </summary>
    public sealed partial class App
    {
        /// <summary>
        /// 
        /// </summary>
        [Inject]
        [NotNull]
        private IJSRuntime? JSRuntime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Inject]
        [NotNull]
        private CurrentUser? CurrentUser { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Microsoft.AspNetCore.Components.Parameter]
        public InitialUser? InitialUser { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override async Task OnInitializedAsync()
        {
            CurrentUser.AccessToken = InitialUser!.AccessToken;
            await base.OnInitializedAsync();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="firstRender"></param>
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            await base.OnAfterRenderAsync(firstRender);

            if (firstRender && OperatingSystem.IsBrowser())
            {
                await JSRuntime.InvokeVoidAsync("$.loading");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Task GlobalException(ILogger logger, Exception ex)
        {
            logger.LogError(ex.Message);
            //在此记录日志
            return Task.CompletedTask;
        }
    }
}