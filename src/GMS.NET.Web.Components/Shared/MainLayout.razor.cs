﻿using GMS.NET.Web.Components.Pages.Systems;
using GMS.NET.Web.Components.Service.Interface.System;
using GMS.NET.Web.Components.Shared.ComtsMisc;
using GMS.NET.Web.Components.Shared.Global.Misc;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Logging;
using System.Diagnostics.CodeAnalysis;

namespace GMS.NET.Web.Components.Shared
{
    /// <summary>
    /// 
    /// </summary>
    public sealed partial class MainLayout
    {
        private bool UseTabSet { get; set; } = true;

        private string Theme { get; set; } = "";

        private bool IsOpen { get; set; }

        private bool IsFixedHeader { get; set; } = true;

        private bool IsFixedFooter { get; set; } = true;

        private bool IsFullSide { get; set; } = false;

        private bool ShowFooter { get; set; } = true;

        private List<MenuItem>? Menus { get; set; }

        [Inject]
        [NotNull]
        private ILayoutService? LayoutService { get; set; }

        [Inject]
        [NotNull]
        private AuthenticationStateProvider? AuthStateProvider { get; set; }
        /// <summary>
        /// OnInitialized 方法
        /// </summary>
        protected override async Task OnInitializedAsync()
        {
            //DispatchService.Subscribe(async (payload) =>
            //{
            //    if (payload.Entry != null)
            //    {
            //        await Toast.Show(new ()
            //        {
            //            Category = ToastCategory.Information,
            //            Title = "消息通知",
            //            Content = payload.Entry
            //        });
            //    }
            //});
            if ((await AuthStateProvider.GetAuthenticationStateAsync()).User.IsAuth())
                await GetIconSideMenuItems();
            await base.OnInitializedAsync();
        }
        private async Task GetIconSideMenuItems()
        {
            Menus = new();

            var menus = await LayoutService.GetMenus();

            //var menus = new List<MenuItemSync>() {
            //    new MenuItemSync() { Text = "返回组件库", Icon = "fa-solid fa-fw fa-home", Url = "https://www.blazor.zone/components" },
            //    new MenuItemSync() { Text = "Index", Icon = "fa-solid fa-fw fa-flag", Url = "/"  },
            //    new MenuItemSync() { Text = "Counter", Icon = "fa-solid fa-fw fa-check-square", Url = "/counter" },
            //    new MenuItemSync() { Text = "FetchData", Icon = "fa-solid fa-fw fa-database", Url = "/fetchdata" },
            //    new MenuItemSync() { Text = "Table", Icon = "fa-solid fa-fw fa-table", Url = "/table" },
            //    new MenuItemSync() { Text = "花名册", Icon = "fa-solid fa-fw fa-users", Url = "/users" },
            //    new MenuItemSync() { Text = "菜单管理", Icon = "fa-solid fa-fw fa-users", Url = "/menus" }
            //};

            if (menus is not null) menus.ForEach(async x => Menus!.Add(await x.RecursiveCopyAsync()));
        }
        //[Inject]
        //[NotNull]
        //private ToastService? Toast { get; set; }
        //[Inject]
        //[NotNull]
        //private IDispatchService<string>? DispatchService { get; set; }
        //[Inject]
        //[NotNull]
        //private WebClientService? ClientService { get; set; }
        /// <summary>
        /// 全局广播
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        //private async Task AllBroadcast(DialButtonItem val)
        //{
        //    var client = await ClientService.GetClientInfo();
        //    var city = client.City ?? "本地";
        //    var ip = client.Ip;
        //    switch (val.Value)
        //    {
        //        case "broadcast":
        //            DispatchService.Dispatch(new DispatchEntry<string>() { Entry=$"{DateTime.Now}  来自{city}用户,Ip{ip}的测试消息" });
        //            break;
        //        default:
        //            break;
        //    }
        //}
    }
}