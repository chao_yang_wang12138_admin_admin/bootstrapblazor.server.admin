﻿namespace GMS.NET.Web.Components.Shared.ComtsMisc
{
    public class MessageAlert
    {
        protected readonly MessageService Message;
        public MessageAlert(MessageService _Message) => this.Message = _Message;
        private async Task Show(string Content, Color color, string Icon)
        {
            await Message.Show(new MessageOption()
            {
                Icon = Icon,
                Color = color,
                Content = Content,
                Delay=4500
            });
        }
        public async Task Success(string Content)=> await Show(Content, Color.Success, "fas fa-fw fa-circle-check");
        public async Task Warning(string Content)=>await Show(Content, Color.Warning, "fas fa-fw fa-circle-exclamation");
        public async Task Error(string Content)=>await Show(Content, Color.Danger, "fas fa-fw fa-circle-xmark");
        public async Task Info(string Content) => await Show(Content, Color.Info, " fas fa-fw fa-circle-info");
    }
}
