﻿using System.ComponentModel.DataAnnotations;
using System.Dynamic;
using DynamicObject = System.Dynamic.DynamicObject;

namespace GMS.NET.Web.Components.Shared.Global.Misc
{
    /// <summary>
    /// 导入excel动态数据
    /// </summary>
    public class DynamicData: DynamicObject
    {
        /// <summary>
        /// 获得/设置 固定列
        /// </summary>
        [Display(Name ="序号")]
        public string Fix { get; set; } = "";

        /// <summary>
        /// 存储每列值信息 Key 列名 Value 为列值
        /// </summary>
        public Dictionary<string, string> Columns { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fix"></param>
        /// <param name="data"></param>
        public DynamicData(string Fix, Dictionary<string, string> data)
        {
            this.Fix = Fix;
            Columns = data;
        }

        /// <summary>
        /// 
        /// </summary>
        public DynamicData() : this("", new()) { }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="binder"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public override bool TryGetMember(GetMemberBinder binder, out object? result)
        {
            if (binder.Name == nameof(Fix))
            {
                result = Fix;
            }
            else if (Columns.ContainsKey(binder.Name))
            {
                result = Columns[binder.Name];
            }
            else
            {
                // When property name not found, return empty
                result = "";
            }
            return true;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="binder"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool TrySetMember(SetMemberBinder binder, object? value)
        {
            var ret = false;
            var v = value?.ToString() ?? string.Empty;
            if (binder.Name == nameof(Fix))
            {
                Fix = v;
                ret = true;
            }
            else if (Columns.ContainsKey(binder.Name))
            {
                Columns[binder.Name] = v;
                ret = true;
            }
            return ret;
        }
    }
}
