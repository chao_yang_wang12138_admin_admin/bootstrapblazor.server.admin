﻿using Microsoft.Extensions.Logging;

namespace GMS.NET.Web.Components.Shared.Global.Misc.Logger
{
    public class LoggerProvider : ILoggerProvider
    {
        private readonly string _logFilePath;
        public LoggerProvider(string logFilePath)
        {
            _logFilePath = logFilePath;
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new LoggerExtensions(_logFilePath);
        }

        public void Dispose()
        {
            // 这里释放任何资源，如果有的话
        }
    }
}
