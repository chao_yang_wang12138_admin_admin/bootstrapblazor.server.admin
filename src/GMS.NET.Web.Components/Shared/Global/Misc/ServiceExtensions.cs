﻿using GMS.NET.Web.Components.Service.HttpApi;
using GMS.NET.Web.Components.Service.Interface.System;
using GMS.NET.Web.Components.Service.Realize.System;
using GMS.NET.Web.Components.Shared.ComtsMisc;
using GMS.NET.Web.Components.Shared.Global.JsInterop.Cookie;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.DependencyInjection;

namespace GMS.NET.Web.Components.Shared.Global.Misc
{
    public static class ServiceExtensions
    {
        public static void AddGMSNETWebService(this IServiceCollection Services, string serveHost)
        {
            // Add Authentication
            Services.AddScoped<CurrentUser>();
            Services.AddScoped<AuthenticationStateProvider, AuthProvider>();

            // Add Misc Util
            Services.AddTransient<ICookie, Cookie>();
            Services.AddTransient<MessageAlert>();

            // Register a webapi request proxy
            Services.AddHttpApi<IAuthClient>();
            Services.AddHttpApi<IToolsClient>();
            Services.AddHttpApi<IMenusClient>();
            Services.AddHttpApi<ILayoutClient>();
            Services.AddHttpApi<IUsersClient>();
            Services.AddHttpApi<IImportClient>();
            Services.AddHttpApi<ICustsClient>();
            Services.AddHttpApi<IAccRolesClient>();
            Services.AddHttpApi<ITestClient>();
            Services.AddHttpApi<ILogsClient>();
            Services.AddWebApiClient().ConfigureHttpApi(x =>
            {
                // x.GlobalFilters.Add(new LogFilter());
                x.HttpHost = new Uri(serveHost);
                x.Properties.Add("Cache-Control", "no-cache");
                x.Properties.Add("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)");
                // 符合国情的不标准时间格式，有些接口就是这么要求必须不标准
                //x.JsonSerializeOptions.Converters.Add(new JsonDateTimeConverter("yyyy-MM-dd HH:mm:ss"));
            });

            // Registering Business Services
            Services.AddScoped<IAuthService, AuthService>();
            Services.AddScoped<IIndexService, IndexService>();
            Services.AddScoped<IToolsService, ToolsService>();
            Services.AddScoped<IMenusService, MenusService>();
            Services.AddScoped<ILayoutService, LayoutService>();
            Services.AddScoped<IUsersService, UsersService>();
            Services.AddScoped<IImportService, ImportService>();
            Services.AddScoped<ICustsService, CustsService>();
            Services.AddScoped<IAccRolesService, AccRolesService>();
            Services.AddScoped<ITestService, TestService>();
            Services.AddScoped<ILogsService, LogsService>();
        }
    }
}
