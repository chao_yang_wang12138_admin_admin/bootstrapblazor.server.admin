﻿namespace GMS.NET.Web.Components.Shared.Global.Misc
{
    public class ClientAppKey
    {
        public const string ServerCookie = ".GMS.NET.AccessToken";
        public const string ServerCode = ".GMS.NET.Encrypt";
    }
}
