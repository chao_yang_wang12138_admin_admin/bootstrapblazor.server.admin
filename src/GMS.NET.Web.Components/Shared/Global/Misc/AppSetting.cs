﻿namespace GMS.NET.Web.Components.Shared.Global.Misc
{
    public class AppSetting
    {
        public const string ServerHost ="ServerHost";
        public const string ServerKey = "ServerKey";
        public const string ServerTools = "ServerTools";
    }
}
