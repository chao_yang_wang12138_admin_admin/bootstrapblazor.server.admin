﻿namespace GMS.NET.Web.Components.Shared.Global.JsInterop.Cookie
{
    public interface ICookie
    {
        /// <summary>
        /// 设置cookie
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        Task SetCookie(string key, string value, int s);
        /// <summary>
        /// 删除cookie
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task RemoveCookie(string key);
        /// <summary>
        /// 获取cookie
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<string> GetCookie(string key);
    }
}
