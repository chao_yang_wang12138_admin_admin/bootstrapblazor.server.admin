﻿using GMS.NET.Encrypt;
using System.Security.Claims;

namespace GMS.NET.Web.Components.Shared.Global.Identity
{
    public static class UserInfo
    {
        /// <summary>
        /// 是否是用户管理员
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public static bool UserAdmin(this ClaimsPrincipal User) =>Convert.ToBoolean(JWT.GetJWTTypeValue(User.AccessToken(), UserClaimConst.UserAdmin));
        /// <summary>
        /// 获取用户账号
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public static string UserId(this ClaimsPrincipal User) => JWT.GetJWTTypeValue(User.AccessToken(), UserClaimConst.UserId);
        /// <summary>
        /// 获取用户名称
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public static string UserName(this ClaimsPrincipal User) => JWT.GetJWTTypeValue(User.AccessToken(), UserClaimConst.UserName);
        /// <summary>
        /// 获取用户token
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public static string AccessToken(this ClaimsPrincipal User) => User is null ? string.Empty : User.FindFirst(UserClaimConst.AccessToken)!.Value;
        /// <summary>
        /// 是否已授权
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public static bool IsAuth(this ClaimsPrincipal User) => User is null ? false : User.Identity!.IsAuthenticated;
    }
}
