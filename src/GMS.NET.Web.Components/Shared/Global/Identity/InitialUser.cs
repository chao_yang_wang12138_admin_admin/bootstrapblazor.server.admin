﻿namespace GMS.NET.Web.Components.Shared.Global.Identity
{
    public class InitialUser
    {
        /// <summary>
        /// 用户令牌
        /// </summary>
        public string? AccessToken { get; set; }
    }
}
