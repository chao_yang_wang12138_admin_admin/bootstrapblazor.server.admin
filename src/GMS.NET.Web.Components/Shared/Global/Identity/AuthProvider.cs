﻿using Microsoft.AspNetCore.Components.Authorization;
using System.Security.Claims;

namespace GMS.NET.Web.Components.Shared.Global.Identity
{
    public class AuthProvider : AuthenticationStateProvider
    {
        private readonly CurrentUser currentUser;
        public AuthProvider(CurrentUser _currentUser) => currentUser = _currentUser;
        public override  Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var Identity = new ClaimsIdentity();
            if (currentUser is not null)
            {
                if (currentUser.AccessToken is not null)
                {
                    List<Claim> claims = new List<Claim>();
                    claims.Add(new Claim(UserClaimConst.AccessToken, currentUser.AccessToken));
                    Identity = new ClaimsIdentity(claims, "IdentityUser");
                }
            }
            return Task.FromResult(new AuthenticationState(new ClaimsPrincipal(Identity)));
        }
    }
}
