﻿using System.Net.Http.Headers;

namespace GMS.NET.Web.Components.Shared.Global.Attributes
{
    /// <summary>
    /// api请求参数类型是文件流
    /// </summary>
    public class ApiRequestStreamAttribute : HttpContentAttribute
    {
        protected override Task SetHttpContentAsync(ApiParameterContext context)
        {
            if (context.ParameterValue is Stream stream)
            {
                var content = new StreamContent(stream);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                context.HttpContext.RequestMessage.Content = content;
            }
            return Task.CompletedTask;
        }
    }
}
