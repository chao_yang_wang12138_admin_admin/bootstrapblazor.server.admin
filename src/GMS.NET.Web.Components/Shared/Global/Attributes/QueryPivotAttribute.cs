﻿namespace GMS.NET.Web.Components.Shared.Global.Attributes
{
    /// <summary>
    /// 表格查询字段配置
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class QueryPivotAttribute : Attribute
    {
        public string? FieldName { get; set; }
        public CSharpType? CSharpTypeName { get; set; }
        public Conditional? ConditionalType { get; set; }
        /// <summary>
        /// 表格查询字段配置
        /// </summary>
        /// <param name="FieldName">字段名称</param>
        /// <param name="CSharpTypeName">字段C#类型</param>
        /// <param name="ConditionalType">字段条件符</param>
        public QueryPivotAttribute(string FieldName, CSharpType CSharpTypeName, Conditional ConditionalType)
        {
            this.FieldName = FieldName;
            this.CSharpTypeName = CSharpTypeName;
            this.ConditionalType = ConditionalType;
        }
        /// <summary>
        /// 表格查询字段配置
        /// </summary>
        /// <param name="CSharpTypeName">字段C#类型</param>
        /// <param name="ConditionalType">字段条件符</param>
        public QueryPivotAttribute(CSharpType CSharpTypeName, Conditional ConditionalType)
        {
            this.CSharpTypeName = CSharpTypeName;
            this.ConditionalType = ConditionalType;
        }
    }
}
