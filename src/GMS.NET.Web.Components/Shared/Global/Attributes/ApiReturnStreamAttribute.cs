﻿using System.Net.Http.Headers;

namespace GMS.NET.Web.Components.Shared.Global.Attributes
{
    /// <summary>
    /// api返回文件流
    /// </summary>
    public class ApiReturnStreamAttribute : ApiReturnAttribute
    {
        public ApiReturnStreamAttribute()
          : base(new MediaTypeWithQualityHeaderValue("application/octet-stream"))
        {
        }

        public override async Task SetResultAsync(ApiResponseContext context)
        {
            var res = await context.HttpContext.ResponseMessage!.Content.ReadAsStreamAsync();
            context.Result = res;
        }
    }
}
