﻿using GMS.NET.Dto.System.Custs.Shared;
using GMS.NET.Web.Components.Service.HttpApi;
using GMS.NET.Web.Components.Service.Interface.System;

namespace GMS.NET.Web.Components.Service.Realize.System
{
    public class CustsService : BaseService, ICustsService
    {
        private readonly ICustsClient _custsClient;
        public CustsService(ICustsClient client, ToastService toast) : base(toast) => _custsClient = client;
        public Task<bool> CheckUsersConfirmOperCustsNotes(bool IsCheck) => ExecuteValueAsync(_custsClient.CheckUsersConfirmOperCustsNotes(IsCheck));
        public Task<bool> CustsStatus(int status, Guid Id) => ExecuteNoValueAsync(_custsClient.CustsStatus(status, Id));
        public Task<bool> DelCustsData(List<Guid> Ids) => ExecuteNoValueAsync(_custsClient.DelCustsData(Ids));
        public Task<TabularData<CustsData>> GetCustsData(QueryPivot queryPivot) => ExecuteValueAsync(_custsClient.GetCustsData(queryPivot));
        public Task<bool> RecoverCustsData(List<Guid> Ids)=> ExecuteNoValueAsync(_custsClient.RecoverCustsData(Ids),"恢复成功");
        public Task<bool> SaveCustsData(CustsData custsData) => ExecuteNoValueAsync(_custsClient.SaveCustsData(custsData));
    }
}
