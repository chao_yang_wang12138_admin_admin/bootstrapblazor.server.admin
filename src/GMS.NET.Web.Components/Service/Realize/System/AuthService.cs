﻿using GMS.NET.Dto.System.Auth.Shared;
using GMS.NET.Dto.System.Auth.Output;
using GMS.NET.Web.Components.Service.HttpApi;
using GMS.NET.Web.Components.Service.Interface.System;

namespace GMS.NET.Web.Components.Service.Realize.System
{
    public class AuthService : BaseService, IAuthService
    {
        private readonly IAuthClient _authClient;
        public AuthService(IAuthClient client, ToastService toast) : base(toast)=> _authClient = client;
        public Task<LoginToken> LoginAsync(LoginAuth user) => ExecuteValueAsync(_authClient.LoginAsync(user));
        public Task<bool> SendPhoneCode(LoginAuth phone) => ExecuteNoValueAsync(_authClient.SendPhoneCode(phone), "发送成功");
        public Task<bool> SendEmailCode(LoginAuth email) => ExecuteNoValueAsync(_authClient.SendEmailCode(email),"发送成功");
    }
}
