﻿using GMS.NET.Dto.System.Layout.Ouput;
using GMS.NET.Web.Components.Service.HttpApi;
using GMS.NET.Web.Components.Service.Interface.System;

namespace GMS.NET.Web.Components.Service.Realize.System
{
    public class LayoutService:BaseService,ILayoutService
    {
        private readonly ILayoutClient _layoutClient;
        public LayoutService(ILayoutClient client, ToastService toast) : base(toast) => _layoutClient = client;
        public Task<List<MenuItemSync>> GetMenus() => ExecuteValueAsync(_layoutClient.GetMenus());
        public Task<bool> IsIndex() => ExecuteValueAsync(_layoutClient.IsIndex());
    }
}
