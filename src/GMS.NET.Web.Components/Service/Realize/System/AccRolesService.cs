﻿using GMS.NET.Dto.System.AccRoles.Input;
using GMS.NET.Dto.System.AccRoles.Output;
using GMS.NET.Dto.System.AccRoles.Shared;
using GMS.NET.Web.Components.Service.HttpApi;
using GMS.NET.Web.Components.Service.Interface.System;

namespace GMS.NET.Web.Components.Service.Realize.System
{
    public class AccRolesService : BaseService, IAccRolesService
    {
        private readonly IAccRolesClient _rolesClient;
        public AccRolesService(IAccRolesClient client, ToastService toast) : base(toast) => _rolesClient = client;
        public Task<bool> CheckRoleNameRepeat(string roleName) => ExecuteValueAsync(_rolesClient.CheckRoleNameRepeat(roleName));
        public Task<bool> DelRolesData(List<Guid> Ids) => ExecuteNoValueAsync(_rolesClient.DelRolesData(Ids));
        public Task<TabularData<RolesData>> GetRolesData(QueryPivot queryPivot) => ExecuteValueAsync(_rolesClient.GetRolesData(queryPivot));
        public Task<List<WaitRess>> GetWaitRess(Guid RoleId) => ExecuteValueAsync(_rolesClient.GetWaitRess(RoleId));
        public Task<TabularData<WaitUsers>> GetWaitUsers(QueryPivot queryPivot) => ExecuteValueAsync(_rolesClient.GetWaitUsers(queryPivot));
        public Task<bool> RoleStatus(int status, Guid Id) => ExecuteNoValueAsync(_rolesClient.RoleStatus(status, Id));
        public Task<bool> SaveRolesData(RolesData rolesData) => ExecuteNoValueAsync(_rolesClient.SaveRolesData(rolesData));
        public Task<bool> SetAccRess(AccRess accRess) => ExecuteNoValueAsync(_rolesClient.SetAccRess(accRess),"分配角色资源成功");
        public Task<bool> SetAccUsers(AccUsers accUsers) => ExecuteNoValueAsync(_rolesClient.SetAccUsers(accUsers),"分配用户角色成功");
    }
}
