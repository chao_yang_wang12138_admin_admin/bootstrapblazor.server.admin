﻿using GMS.NET.Dto.System.Test.Output;
using GMS.NET.Web.Components.Service.HttpApi;
using GMS.NET.Web.Components.Service.Interface.System;

namespace GMS.NET.Web.Components.Service.Realize.System
{
    public class TestService : BaseService,ITestService
    {
        private readonly ITestClient _testClient;
        public TestService(ITestClient client, ToastService toast) : base(toast) => _testClient = client;
        public Task<TabularData<TestData>> GetTestData(QueryPivot query) => ExecuteValueAsync(_testClient.GetTestData(query));
    }
}
