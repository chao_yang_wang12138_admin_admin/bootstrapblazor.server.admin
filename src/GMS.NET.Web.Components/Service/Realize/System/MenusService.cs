﻿using GMS.NET.Dto.System.Menus.Shared;
using GMS.NET.Web.Components.Service.HttpApi;
using GMS.NET.Web.Components.Service.Interface.System;

namespace GMS.NET.Web.Components.Service.Realize.System
{
    public class MenusService :BaseService,IMenusService
    {
        private readonly IMenusClient _menusClient;
        public MenusService(IMenusClient client, ToastService toast) : base(toast) => _menusClient = client;
        public Task<bool> DelMenusTree(List<Guid> Id) => ExecuteNoValueAsync(_menusClient.DelMenusTree(Id));
        public Task<CodeCSInfo> GetCodeInfo(Guid Id) => ExecuteValueAsync(_menusClient.GetCodeInfo(Id));
        public Task<List<MenusTree>> GetMenusTree(Guid ParentId) => ExecuteValueAsync(_menusClient.GetMenusTree(ParentId));
        public Task<List<KeyValues>> GetParentList(string Text)=>ExecuteValueAsync(_menusClient.GetParentList(Text));
        public Task<bool> MenusTreeStatus(int status, Guid Id) => ExecuteNoValueAsync(_menusClient.MenusTreeStatus(status, Id));
        public Task<bool> SaveMenusTree(MenusTree menusTree) => ExecuteNoValueAsync(_menusClient.SaveMenusTree(menusTree));
    }
}
