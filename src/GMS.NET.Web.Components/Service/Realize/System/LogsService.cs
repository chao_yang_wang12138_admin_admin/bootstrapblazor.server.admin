﻿using GMS.NET.Dto.System.Logs.Output;
using GMS.NET.Web.Components.Service.HttpApi;
using GMS.NET.Web.Components.Service.Interface.System;

namespace GMS.NET.Web.Components.Service.Realize.System
{
    public class LogsService:BaseService,ILogsService
    {
        private readonly ILogsClient _logsClient;
        public LogsService(ILogsClient client, ToastService toast) : base(toast) => _logsClient = client;
        public Task<TabularData<LogData>> GetLogData(QueryPivot query) => ExecuteValueAsync(_logsClient.GetLogData(query));
    }
}
