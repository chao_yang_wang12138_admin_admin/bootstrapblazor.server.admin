﻿using GMS.NET.Dto.System.Tools.Input;
using GMS.NET.Web.Components.Service.HttpApi;
using GMS.NET.Web.Components.Service.Interface.System;

namespace GMS.NET.Web.Components.Service.Realize.System
{
    public class ToolsService:BaseService,IToolsService
    {
        private readonly IToolsClient _toolsClient;
        public ToolsService(ToastService toast, IToolsClient client) : base(toast) => this._toolsClient = client;
        // 数据表管理工具
        public Task<bool> CreateExcelToEntitysFileAndDbTable(Stream stream,string nameSpace) =>
            ExecuteNoValueAsync(_toolsClient.CreateExcelToEntitysFileAndDbTable(stream, nameSpace), "数据实体已创建或同步");
        public Task<MemoryStream> DatabaseExcelTempStream() =>
            ExecuteValueAsync(_toolsClient.DatabaseExcelTempStream());
        public Task<bool> EntitysToDatabaseTable(string nameSpace) => 
            ExecuteNoValueAsync(_toolsClient.EntitysToDatabaseTable(nameSpace), "数据实体已创建或同步");
        // 系统数据恢复工具
        public Task<bool> FullRecovery() => ExecuteNoValueAsync(_toolsClient.FullRecovery(), "恢复数据成功");
        // 数据种子创建工具
        public Task<bool> CreateDataBaseToDataSeedEntitys(DataSeedDescr dataSeedDescr) => 
            ExecuteNoValueAsync(_toolsClient.CreateDataBaseToDataSeedEntitys(dataSeedDescr),"种子数据创建成功");

    }
}
