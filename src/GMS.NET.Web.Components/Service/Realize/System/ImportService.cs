﻿using GMS.NET.Web.Components.Service.HttpApi;
using GMS.NET.Web.Components.Service.Interface.System;

namespace GMS.NET.Web.Components.Service.Realize.System
{
    public class ImportService : BaseService, IImportService
    {
        private readonly IImportClient _importClient;
        public ImportService(IImportClient client, ToastService toast) : base(toast) => _importClient = client;
        public Task<bool> CheckExcelData(string requestUrl) => ExecuteNoValueAsync(_importClient.CheckExcelData(requestUrl),"数据检验成功");
        public Task<MemoryStream> GetExcelCheckData()=>ExecuteValueAsync(_importClient.GetExcelCheckData());
        public Task<TabularData<Dictionary<string, string>>> GetExcelData(QueryPivot queryPivot) => ExecuteValueAsync(_importClient.GetExcelData(queryPivot));
        public Task<MemoryStream> GetExcelTemp(string requestUrl) =>ExecuteValueAsync(_importClient.GetExcelTemp(requestUrl));
        public Task<bool> ImportExcelData(string requestUrl)=>ExecuteNoValueAsync(_importClient.ImportExcelData(requestUrl),"数据导入成功");
        public Task<List<string>> UploadExcelData(Stream stream) =>ExecuteValueAsync(_importClient.UploadExcelData(stream));
    }
}
