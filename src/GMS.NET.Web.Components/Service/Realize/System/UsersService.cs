﻿using GMS.NET.Dto.System.Users.Shared;
using GMS.NET.Dto.System.Users.UserCenter.Shared;
using GMS.NET.Web.Components.Service.HttpApi;
using GMS.NET.Web.Components.Service.Interface.System;

namespace GMS.NET.Web.Components.Service.Realize.System
{
    public class UsersService : BaseService, IUsersService
    {
        private readonly IUsersClient _usersClient;
        public UsersService(ToastService toast, IUsersClient client) : base(toast) => this._usersClient = client;
        public Task<bool> DelUser([JsonContent] List<Guid> Ids) => ExecuteNoValueAsync(_usersClient.DelUser(Ids));
        public Task<List<KeyValues>> GetCustsList(string Text) => ExecuteValueAsync(_usersClient.GetCustsList(Text));
        public Task<List<KeyValues>> GetRolesList(string Text) => ExecuteValueAsync(_usersClient.GetRolesList(Text));
        public Task<TabularData<UsersData>> GetUsers(QueryPivot queryPivot) =>ExecuteValueAsync(_usersClient.GetUsers(queryPivot));
        public Task<bool> SaveUser([JsonContent] UsersData usersData)=>ExecuteNoValueAsync(_usersClient.SaveUser(usersData));
        public Task<bool> UserStatus(int status, Guid Id) => ExecuteNoValueAsync(_usersClient.UserStatus(status, Id));
        //usercenter
        public Task<UserInfoCenter> GetUserInfo() => ExecuteValueAsync(_usersClient.GetUserInfo());
        public Task<bool> UpdUserPas(string Coding) => ExecuteNoValueAsync(_usersClient.UpdUserPas(Coding),"修改成功,下次登陆请使用新密码");
        public Task<bool> CheckOldPas(string Coding) => ExecuteValueAsync(_usersClient.CheckOldPas(Coding));
    }
}
