﻿using GMS.NET.Dto.System.Logs.Output;

namespace GMS.NET.Web.Components.Service.Interface.System
{
    public interface ILogsService
    {
        Task<TabularData<LogData>> GetLogData(QueryPivot query);
    }
}
