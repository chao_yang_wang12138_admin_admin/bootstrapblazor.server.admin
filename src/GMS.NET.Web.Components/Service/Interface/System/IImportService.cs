﻿namespace GMS.NET.Web.Components.Service.Interface.System
{
    public interface IImportService
    {
        /// <summary>
        /// 获取excel上传模板
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <returns></returns>
        Task<MemoryStream> GetExcelTemp(string requestUrl);
        /// <summary>
        /// 上传Excel的数据
        /// </summary>
        /// <returns></returns>
        Task<List<string>> UploadExcelData(Stream stream);
        /// <summary>
        /// 获取excel上传的数据
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <returns></returns>
        Task<TabularData<Dictionary<string, string>>> GetExcelData(QueryPivot queryPivot);
        /// <summary>
        /// 检验excel数据
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <returns></returns>
        Task<bool> CheckExcelData(string requestUrl);
        /// <summary>
        /// 获取excel检验结果
        /// </summary>
        /// <returns></returns>
        Task<MemoryStream> GetExcelCheckData();
        /// <summary>
        /// 导入excel数据
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <returns></returns>
        Task<bool> ImportExcelData(string requestUrl);
    }
}
