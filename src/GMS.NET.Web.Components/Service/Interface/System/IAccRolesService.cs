﻿using GMS.NET.Dto.System.AccRoles.Input;
using GMS.NET.Dto.System.AccRoles.Output;
using GMS.NET.Dto.System.AccRoles.Shared;

namespace GMS.NET.Web.Components.Service.Interface.System
{
    public interface IAccRolesService
    {
        Task<bool> DelRolesData(List<Guid> Ids);
        Task<bool> SaveRolesData(RolesData rolesData);
        Task<bool> CheckRoleNameRepeat(string roleName);
        Task<TabularData<RolesData>> GetRolesData(QueryPivot queryPivot);
        Task<TabularData<WaitUsers>> GetWaitUsers(QueryPivot queryPivot);
        Task<List<WaitRess>> GetWaitRess(Guid RoleId);
        Task<bool> SetAccUsers(AccUsers accUsers);
        Task<bool> SetAccRess(AccRess accRess);
        Task<bool> RoleStatus(int status, Guid Id);
    }
}