﻿using GMS.NET.Dto.Basic;
using GMS.NET.Dto.System.Menus.Enums;
using GMS.NET.Dto.System.Menus.Shared;

namespace GMS.NET.Web.Components.Service.Interface.System
{
    public interface IMenusService
    {
        Task<List<MenusTree>> GetMenusTree(Guid ParentId);
        Task<bool> SaveMenusTree(MenusTree menusTree);
        Task<bool> DelMenusTree(List<Guid> Id);
        Task<List<KeyValues>> GetParentList(string Text);
        Task<bool> MenusTreeStatus(int status, Guid Id);
        Task<CodeCSInfo> GetCodeInfo(Guid Id);
    }
}
