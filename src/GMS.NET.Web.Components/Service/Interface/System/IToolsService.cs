﻿using GMS.NET.Dto.System.Tools.Input;

namespace GMS.NET.Web.Components.Service.Interface.System
{
    public interface IToolsService
    {
        // 数据表管理工具
        Task<bool> EntitysToDatabaseTable(string nameSpace);
        Task<MemoryStream> DatabaseExcelTempStream();
        Task<bool> CreateExcelToEntitysFileAndDbTable(Stream stream, string nameSpace);
        // 系统数据恢复工具
        Task<bool> FullRecovery();
        // 数据库种子创建工具
        Task<bool> CreateDataBaseToDataSeedEntitys(DataSeedDescr dataSeedDescr);
    }
}
