﻿
using GMS.NET.Dto.System.Custs.Shared;

namespace GMS.NET.Web.Components.Service.Interface.System
{
    public interface ICustsService
    {
        Task<bool> CustsStatus(int status, Guid Id);
        Task<TabularData<CustsData>> GetCustsData(QueryPivot queryPivot);
        Task<bool> SaveCustsData(CustsData custsData);
        Task<bool> DelCustsData(List<Guid> Ids);
        Task<bool> RecoverCustsData(List<Guid> Ids);
        Task<bool> CheckUsersConfirmOperCustsNotes(bool IsCheck);
    }
}
