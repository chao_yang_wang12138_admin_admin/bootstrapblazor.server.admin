﻿using GMS.NET.Dto.System.AccRoles.Input;
using GMS.NET.Dto.System.Users.Shared;
using GMS.NET.Dto.System.Users.UserCenter.Shared;

namespace GMS.NET.Web.Components.Service.Interface.System
{
    public interface IUsersService
    {
        Task<bool> UserStatus(int status,Guid Id);
        Task<TabularData<UsersData>> GetUsers(QueryPivot queryPivot);
        Task<bool> DelUser([JsonContent] List<Guid> Ids);
        Task<bool> SaveUser([JsonContent] UsersData usersData);
        Task<List<KeyValues>> GetRolesList(string Text);
        Task<List<KeyValues>> GetCustsList(string Text);
        Task<UserInfoCenter> GetUserInfo();
        Task<bool> UpdUserPas(string Coding);
        Task<bool> CheckOldPas(string Coding);
    }
}
