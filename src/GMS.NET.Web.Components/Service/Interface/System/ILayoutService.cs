﻿
using GMS.NET.Dto.System.Layout.Ouput;

namespace GMS.NET.Web.Components.Service.Interface.System
{
    public interface ILayoutService
    {
        Task<List<MenuItemSync>> GetMenus();
        Task<bool> IsIndex();
    }
}
