﻿using GMS.NET.Dto.System.Test.Output;

namespace GMS.NET.Web.Components.Service.Interface.System
{
    public interface ITestService
    {
        Task<TabularData<TestData>> GetTestData(QueryPivot query);
    }
}
