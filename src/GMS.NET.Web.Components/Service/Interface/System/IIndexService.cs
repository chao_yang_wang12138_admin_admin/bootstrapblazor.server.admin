﻿using GMS.NET.Dto.System.Index.Output;

namespace GMS.NET.Web.Components.Service.Interface.System
{
    public interface IIndexService
    {
        Task<DashboardData> GetDashboardDataAsync(DateTime dateTime);
    }
}
