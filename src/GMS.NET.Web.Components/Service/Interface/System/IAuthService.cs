﻿using GMS.NET.Dto.System.Auth.Shared;
using GMS.NET.Dto.System.Auth.Output;

namespace GMS.NET.Web.Components.Service.Interface.System
{
    public interface IAuthService
    {
        Task<LoginToken> LoginAsync(LoginAuth user);
        Task<bool> SendEmailCode(LoginAuth email);
        Task<bool> SendPhoneCode(LoginAuth phone);
    }
}
