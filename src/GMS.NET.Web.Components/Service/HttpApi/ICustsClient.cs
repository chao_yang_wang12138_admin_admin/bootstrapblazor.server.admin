﻿using GMS.NET.Dto.System.Custs.Shared;

namespace GMS.NET.Web.Components.Service.HttpApi
{
    [ApiSecurity]
    public interface ICustsClient
    {
        /// <summary>
        /// 获取客户信息
        /// </summary>
        /// <param name="queryPivot"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/custs/custs-data")]
        ITask<RestfulResult<TabularData<CustsData>>> GetCustsData([JsonContent] QueryPivot queryPivot, CancellationToken token = default);
        /// <summary>
        /// 保存客户信息
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/custs/save-custs-data")]
        ITask<RestfulResult<object>> SaveCustsData([JsonContent] CustsData custsData, CancellationToken token = default);
        /// <summary>
        /// 删除客户信息
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/custs/del-custs-data")]
        ITask<RestfulResult<object>> DelCustsData([JsonContent] List<Guid> Ids, CancellationToken token = default);
        /// <summary>
        /// 恢复客户信息
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/custs/recover-custs-data")]
        ITask<RestfulResult<object>> RecoverCustsData([JsonContent] List<Guid> Ids, CancellationToken token = default);
        /// <summary>
        /// 检测用户时候已经了解注册客户注意事项
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/custs/check-users-confirm-oper-custs-notes")]
        ITask<RestfulResult<bool>> CheckUsersConfirmOperCustsNotes([JsonContent] bool IsCheck,CancellationToken token = default);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="status"></param>
        /// <param name="Id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/custs/custs-status/{status}/{Id}")]
        ITask<RestfulResult<object>> CustsStatus([PathQuery] int status, [PathQuery] Guid Id, CancellationToken token = default);
    }
}
