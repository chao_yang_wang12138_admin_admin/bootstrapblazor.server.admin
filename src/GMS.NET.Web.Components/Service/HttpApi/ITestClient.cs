﻿using GMS.NET.Dto.System.Test.Output;

namespace GMS.NET.Web.Components.Service.HttpApi
{
    [ApiSecurity]
    public interface ITestClient: IHttpApi
    {
        [HttpPost("api/test/test-data")]
        ITask<RestfulResult<TabularData<TestData>>> GetTestData([JsonContent] QueryPivot query, CancellationToken token = default);
    }
}
