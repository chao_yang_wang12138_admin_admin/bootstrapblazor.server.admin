﻿using GMS.NET.Dto.System.Logs.Output;

namespace GMS.NET.Web.Components.Service.HttpApi
{
    [ApiSecurity]
    public interface ILogsClient:IHttpApi
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/logs/log-data")]
        ITask<RestfulResult<TabularData<LogData>>> GetLogData([JsonContent] QueryPivot query, CancellationToken token = default);
    }
}
