﻿namespace GMS.NET.Web.Components.Service.HttpApi
{
    [ApiSecurity]
    public interface IImportClient
    {
        /// <summary>
        /// 获取excel上传模板
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiDynamicUrl]
        [ApiReturnStream]
        ITask<MemoryStream> GetExcelTemp([ApiUrl] string requestUrl,CancellationToken token = default);
        /// <summary>
        /// 上传Excel的数据
        /// </summary>
        /// <returns></returns>
        [HttpPost("api/import/upload-excel-data")]
        ITask<RestfulResult<List<string>>> UploadExcelData([ApiRequestStream] Stream stream, CancellationToken token = default);
        /// <summary>
        /// 获取excel上传的数据
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <returns></returns>
        [HttpPost("api/import/excel-data")]
        ITask<RestfulResult<TabularData<Dictionary<string,string>>>> GetExcelData([JsonContent] QueryPivot queryPivot,CancellationToken token = default);
        /// <summary>
        /// 检验excel数据
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiDynamicUrl]
        ITask<RestfulResult<object>> CheckExcelData([ApiUrl] string requestUrl, CancellationToken token = default);
        /// <summary>
        /// 获取excel检验结果
        /// </summary>
        /// <returns></returns>
        [ApiReturnStream]
        [HttpPost("api/import/excel-check-data")]
        ITask<MemoryStream> GetExcelCheckData(CancellationToken token = default);
        /// <summary>
        /// 导入excel数据
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiDynamicUrl]
        ITask<RestfulResult<object>> ImportExcelData([ApiUrl] string requestUrl, CancellationToken token = default);
    }
}
