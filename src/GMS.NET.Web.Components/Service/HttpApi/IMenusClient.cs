﻿using GMS.NET.Dto.System.Menus.Enums;
using GMS.NET.Dto.System.Menus.Shared;

namespace GMS.NET.Web.Components.Service.HttpApi
{
    [ApiSecurity]
    public interface IMenusClient
    {
        [HttpPost("api/menus/menus-tree")]
        ITask<RestfulResult<List<MenusTree>>> GetMenusTree([JsonContent] Guid ParentId, CancellationToken token = default);
        [HttpPost("api/menus/save-menus-tree")]
        ITask<RestfulResult<object>> SaveMenusTree([JsonContent] MenusTree menusTree, CancellationToken token = default);
        [HttpPost("api/menus/del-menus-tree")]
        ITask<RestfulResult<object>> DelMenusTree([JsonContent] List<Guid> Id, CancellationToken token = default);
        [HttpPost("api/menus/parent-list")]
        ITask<RestfulResult<List<KeyValues>>> GetParentList([JsonContent] string Text, CancellationToken token = default);
        [HttpPost("api/menus/menus-tree-status/{status}/{id}")]
        ITask<RestfulResult<object>> MenusTreeStatus([PathQuery] int status, [PathQuery] Guid id ,CancellationToken token = default);
        [HttpPost("api/menus/code-info/{id}")]
        ITask<RestfulResult<CodeCSInfo>> GetCodeInfo([PathQuery] Guid id, CancellationToken token = default);
    }
}
