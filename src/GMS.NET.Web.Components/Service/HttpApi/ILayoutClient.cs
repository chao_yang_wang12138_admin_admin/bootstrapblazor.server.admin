﻿using GMS.NET.Dto.System.Layout.Ouput;

namespace GMS.NET.Web.Components.Service.HttpApi
{
    [ApiSecurity]
    public interface ILayoutClient
    {
        [HttpPost("api/layout/menus")]
        ITask<RestfulResult<List<MenuItemSync>>> GetMenus(CancellationToken token = default);
        [HttpPost("api/layout/is-index")]
        ITask<RestfulResult<bool>> IsIndex(CancellationToken token = default);
    }
}
