﻿using GMS.NET.Dto.System.Tools.Input;

namespace GMS.NET.Web.Components.Service.HttpApi
{
    [ApiSecurity(false)]
    public interface IToolsClient: IHttpApi
    {
        // 数据表管理工具
        [HttpPost("api/database/entitys-to-database-table")]
        ITask<RestfulResult<object>> EntitysToDatabaseTable([JsonContent] string nameSpace,CancellationToken token=default);
        [ApiReturnStream]
        [HttpPost("api/database/database-excel-temp-stream")]
        ITask<MemoryStream> DatabaseExcelTempStream(CancellationToken token = default);
        [HttpPost("api/database/excel-to-entitys-file-and-db-table")]
        ITask<RestfulResult<object>> CreateExcelToEntitysFileAndDbTable([ApiRequestStream] Stream stream, [PathQuery] string nameSpace, CancellationToken token = default);
        // 系统数据恢复工具
        [HttpPost("api/data-recovery/full-recovery")]
        ITask<RestfulResult<object>> FullRecovery(CancellationToken token = default);
        // 数据种子创建工具
        [HttpPost("api/data-seed/data-base-to-data-seed-entitys")]
        ITask<RestfulResult<object>> CreateDataBaseToDataSeedEntitys([JsonContent] DataSeedDescr dataSeedDescr, CancellationToken token = default);
    }
}
