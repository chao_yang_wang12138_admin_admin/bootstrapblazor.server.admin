﻿using GMS.NET.Dto.System.AccRoles.Input;
using GMS.NET.Dto.System.Users.Shared;
using GMS.NET.Dto.System.Users.UserCenter.Shared;

namespace GMS.NET.Web.Components.Service.HttpApi
{
    [ApiSecurity]
    public interface IUsersClient : IHttpApi
    {
        [HttpPost("api/users/users")]
        ITask<RestfulResult<TabularData<UsersData>>> GetUsers([JsonContent] QueryPivot queryPivot, CancellationToken token = default);
        [HttpPost("api/users/del-user")]
        ITask<RestfulResult<object>> DelUser([JsonContent] List<Guid> Ids, CancellationToken token = default);
        [HttpPost("api/users/save-user")]
        ITask<RestfulResult<object>> SaveUser([JsonContent] UsersData usersData, CancellationToken token = default);
        [HttpPost("api/users/roles-list")]
        ITask<RestfulResult<List<KeyValues>>> GetRolesList([JsonContent] string Text, CancellationToken token = default);
        [HttpPost("api/users/custs-list")]
        ITask<RestfulResult<List<KeyValues>>> GetCustsList([JsonContent] string Text, CancellationToken token = default);
        [HttpPost("api/users/user-status/{status}/{Id}")]
        ITask<RestfulResult<object>> UserStatus([PathQuery] int status, [PathQuery] Guid Id, CancellationToken token = default);
        [HttpPost("api/user-center/user-info")]
        ITask<RestfulResult<UserInfoCenter>> GetUserInfo(CancellationToken token = default);
        [HttpPost("api/user-center/upd-user-pas")]
        ITask<RestfulResult<object>> UpdUserPas([JsonContent] string Coding,CancellationToken token = default);
        [HttpPost("api/user-center/check-old-pas")]
        ITask<RestfulResult<bool>> CheckOldPas([JsonContent] string Coding, CancellationToken token = default);
        

    }
}
