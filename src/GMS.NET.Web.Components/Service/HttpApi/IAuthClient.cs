﻿using GMS.NET.Dto.System.Auth.Shared;
using GMS.NET.Dto.System.Auth.Output;

namespace GMS.NET.Web.Components.Service.HttpApi
{
    [ApiSecurity(false)]
    public interface IAuthClient: IHttpApi
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="body"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [ApiReturnHeader(typeof(LoginToken))]
        [HttpPost("api/auth/login-auth")]
        ITask<RestfulResult<LoginToken>> LoginAsync([JsonContent] LoginAuth user, CancellationToken token = default);
        /// <summary>
        /// 发送邮箱验证码
        /// </summary>
        /// <param name="emailcode"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/auth/send-email-code")]
        ITask<RestfulResult<object>> SendEmailCode([JsonContent] LoginAuth email, CancellationToken token = default);
        /// <summary>
        /// 发送手机验证码
        /// </summary>
        /// <param name="emailcode"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/auth/send-phone-code")]
        ITask<RestfulResult<object>> SendPhoneCode([JsonContent] LoginAuth phone, CancellationToken token = default);

    }
}
