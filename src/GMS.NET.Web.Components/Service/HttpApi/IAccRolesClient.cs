﻿using GMS.NET.Dto.System.AccRoles.Input;
using GMS.NET.Dto.System.AccRoles.Output;
using GMS.NET.Dto.System.AccRoles.Shared;

namespace GMS.NET.Web.Components.Service.HttpApi
{
    [ApiSecurity]
    public interface IAccRolesClient
    {
        /// <summary>
        /// 检测角色名称是否重复
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/acc-roles/check-role-name-repeat")]
        ITask<RestfulResult<bool>> CheckRoleNameRepeat([JsonContent] string roleName, CancellationToken token = default);
        /// <summary>
        /// 保存角色信息
        /// </summary>
        /// <param name="rolesData"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/acc-roles/save-roles-data")]
        ITask<RestfulResult<object>> SaveRolesData([JsonContent] RolesData rolesData, CancellationToken token = default);
        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="Ids"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/acc-roles/del-roles-data")]
        ITask<RestfulResult<object>> DelRolesData([JsonContent] List<Guid> Ids, CancellationToken token = default);
        /// <summary>
        /// 获取角色信息
        /// </summary>
        /// <param name="queryPivot"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/acc-roles/roles-data")]
        ITask<RestfulResult<TabularData<RolesData>>> GetRolesData([JsonContent] QueryPivot queryPivot, CancellationToken token = default);
        /// <summary>
        /// 获取该角色下可选用户
        /// </summary>
        /// <param name="queryPivot"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/acc-roles/wait-users")]
        ITask<RestfulResult<TabularData<WaitUsers>>> GetWaitUsers([JsonContent] QueryPivot queryPivot, CancellationToken token = default);
        /// <summary>
        /// 获取该角色下可选资源
        /// </summary>
        /// <param name="ressParam"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/acc-roles/wait-ress")]
        ITask<RestfulResult<List<WaitRess>>> GetWaitRess([JsonContent] Guid RoleId, CancellationToken token = default);
        /// <summary>
        /// 批量设置用户角色
        /// </summary>
        /// <param name="accUser"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/acc-roles/set-acc-users")]
        ITask<RestfulResult<object>> SetAccUsers([JsonContent] AccUsers accUser, CancellationToken token = default);
        /// <summary>
        /// 批量设置角色资源
        /// </summary>
        /// <param name="accUser"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/acc-roles/set-acc-ress")]
        ITask<RestfulResult<object>> SetAccRess([JsonContent] AccRess accRess, CancellationToken token = default);
        /// <summary>
        /// 设置角色状态
        /// </summary>
        /// <param name="accUser"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("api/acc-roles/role-status/{status}/{Id}")]
        ITask<RestfulResult<object>> RoleStatus([PathQuery] int status, [PathQuery] Guid Id, CancellationToken token = default);
        
    }
}
