﻿
namespace GMS.NET.Web.Components.Service
{
    /// <summary>
    /// 业务方法封装
    /// </summary>
    public abstract class BaseService
    {
        protected readonly ToastService Toast;
        public BaseService(ToastService toast) => Toast = toast;

        /// <summary>
        /// 无返回值 操作类
        /// 新增丶删除丶修改等
        /// 规范化结果
        /// </summary>
        /// <param name="task">http请求任务</param>
        /// <param name="successMessage">任务执行成功消息</param>
        /// <param name="useMethtips">是否使用预设http响应消息提示</param>
        /// <returns></returns>
        protected async Task<bool> ExecuteNoValueAsync<TResult>(ITask<RestfulResult<TResult>> task, string successMessage = "")
        {
            try
            {
                var result = await task;
                if (result.Succeeded)
                {
                    if (!string.IsNullOrWhiteSpace(successMessage)) await Toast.Success("操作成功", $"{successMessage}，4秒后自动关闭");
                    return true;
                }
                else
                {
                    await Toast.Error("操作失败", result.Errors!.ToString()!);
                    return false;
                }
            }
            catch (HttpRequestException requestex)
            {
                await Toast.Error("操作失败", requestex.Message);
                return false;
            }
        }
        /// <summary>
        /// 有返回值操作类
        /// 规范化结果
        /// </summary>
        /// <param name="task">http请求任务</param>
        /// <returns></returns>
        protected async Task<TResult> ExecuteValueAsync<TResult>(ITask<RestfulResult<TResult>> task)
        {
            try
            {
                var result = await task;
                if (!result.Succeeded)
                    await Toast.Error("操作失败", result.Errors!.ToString()!);
                return result.Data is null ? Activator.CreateInstance<TResult>() : result.Data;
            }
            catch (HttpRequestException requestex)
            {
                await Toast.Error("操作失败", requestex.Message);
                return Activator.CreateInstance<TResult>();
            }
        }
        /// <summary>
        /// 有返回值操作类
        /// 非规范化结果
        /// </summary>
        /// <param name="task">http请求任务</param>
        /// <returns></returns>
        protected async Task<TResult> ExecuteValueAsync<TResult>(ITask<TResult> task)
        {
            try
            {
                var result = await task;
                if (result is null)
                    await Toast.Error("操作失败", "返回结果为null");
                return result is null ? typeof(TResult) == typeof(MemoryStream) ? (TResult)(object)new MemoryStream() : Activator.CreateInstance<TResult>() : result;
            }
            catch (HttpRequestException requestex)
            {
                await Toast.Error("操作失败", requestex.Message);
                return typeof(TResult) == typeof(MemoryStream) ? (TResult)(object)new MemoryStream() : Activator.CreateInstance<TResult>();
            }
        }
    }
}
