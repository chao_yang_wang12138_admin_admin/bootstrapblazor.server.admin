﻿using GMS.NET.Dto.System.Custs.Shared;
using GMS.NET.Dto.System.Users.UserCenter.Input;
using GMS.NET.Dto.System.Users.UserCenter.Shared;
using GMS.NET.Encrypt;
using GMS.NET.Web.Components.Service.Interface.System;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.Extensions.Configuration;
using System.Diagnostics.CodeAnalysis;

namespace GMS.NET.Web.Components.Pages.Systems.Users
{
    public partial class UserCenter
    {
        [Inject]
        [NotNull]
        private WebClientService? ClientService { get; set; }
        [Inject]
        [NotNull]
        private IUsersService? UsersService { get; set; }
        [Inject]
        [NotNull]
        private IConfiguration? app { get; set; }
        private UserInfoCenter? InfoCenter { get; set; }
        /// <summary>
        /// 表单验证规则
        /// </summary>
        private List<IValidator> UserDataRules { get; } = new();
        protected override async Task OnInitializedAsync()
        {
            InfoCenter = await UsersService.GetUserInfo();
            var clientInfo = await ClientService.GetClientInfo();
            InfoCenter.User_Ip = clientInfo.Ip;
            InfoCenter.User_Address = clientInfo.City;
            UserDataRules.Add(new FormItemValidator(new FormVaildationAttribute( (value, context) =>
            {
                switch (context.MemberName)
                {
                    case "NewPassWord":
                        var objEt = context.ObjectInstance as UserInfoPas;
                        if (objEt!.NewPassWordTo != value.ToString() && objEt!.NewPassWordTo != null)
                            return (false, "两次密码输入不一致");
                        break;
                    case "NewPassWordTo":
                        var objEt2 = context.ObjectInstance as UserInfoPas;
                        if (objEt2!.NewPassWord != value.ToString() && objEt2!.NewPassWord != null)
                            return (false, "两次密码输入不一致");
                        break;
                    case "OldPassWord":
                        var publickey = app.GetSection(EncryptConst.PublicKey).Value ?? "";
                        var coding = RSA.Encrypt(value.ToString()!, publickey);
                        var isVerify = Task.Run(async () =>await UsersService.CheckOldPas(coding)).GetAwaiter().GetResult();
                        if (!isVerify) return (false, "原密码错误");
                        break;
                }
                return FormVaildationResult.Success;
            })));
            await base.OnInitializedAsync();
        }
        private bool saveUserbtndis = true;
        private void OnFiledChanged(string str, object obj) => saveUserbtndis = false;
        [NotNull]
        private Modal? PasModal { get; set; }
        private UserInfoPas? userInfoPas { get; set; }
        ValidateForm? PasFormref;
        private async Task PasUpdModal()
        {
            userInfoPas = new();
            await PasModal.Toggle();
        }
        private async Task PasUpdAction()
        {
            var isValidate = PasFormref!.Validate();
            if (isValidate)
            {
                var publickey = app.GetSection(EncryptConst.PublicKey).Value ?? "";
                var coding = RSA.Encrypt(userInfoPas!.NewPassWord!, publickey);
                await UsersService.UpdUserPas(coding); 
                await PasModal.Toggle();
            }
        }
        private Task SendPhoneCode()
        {
            return Task.CompletedTask;
        }
        private Task SendEmailCode()
        {
            return Task.CompletedTask;
        }
        private Task UpdUserInfo()
        {
            return Task.CompletedTask;
        }
    }
}
