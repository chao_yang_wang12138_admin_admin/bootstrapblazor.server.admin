﻿using GMS.NET.Dto.System.Users.Shared;

namespace GMS.NET.Web.Components.Pages.Systems.Users.Shared
{
    public class UsersSearchModel : ITableSearchModel
    {
        [QueryPivot(CSharpType.String, Conditional.Like)]
        public string? User_Account { get; set; }
        [QueryPivot(CSharpType.String, Conditional.Like)]
        public string? User_Name { get; set; }
        public IEnumerable<IFilterAction> GetSearches()
        {
            var ret = new List<IFilterAction>();
            if (!string.IsNullOrWhiteSpace(User_Account))
                ret.Add(new SearchFilterAction(nameof(UsersData.User_Account), User_Account));
            if (!string.IsNullOrWhiteSpace(User_Name))
                ret.Add(new SearchFilterAction(nameof(UsersData.User_Name), User_Name));
            return ret;
        }
        public void Reset()
        {
            this.User_Name = null;
            this.User_Account = null;
        }
    }
}
