﻿using GMS.NET.Dto.System.Users.Enums;
using GMS.NET.Dto.System.Users.Shared;
using GMS.NET.Encrypt;
using GMS.NET.Web.Components.Pages.Systems.Users.Shared;
using GMS.NET.Web.Components.Service.Interface.System;
using GMS.NET.Web.Components.Shared.Global.Misc;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using System.Diagnostics.CodeAnalysis;

namespace GMS.NET.Web.Components.Pages.Systems.Users
{
    public partial class Users
    {
        protected override Task OnInitializedAsync()
        {
            return base.OnInitializedAsync();
        }

        [Inject]
        [NotNull]
        private IUsersService? UsersService { get; set; }
        [Inject]
        [NotNull]
        private MessageService? MessageService { get; set; }
        [Inject]
        [NotNull]
        private ITableExcelExport? Exporter { get; set; }
        [Inject]
        [NotNull]
        private IConfiguration? app { get; set; }
        private async Task UserStatusOnChanged(bool val, Guid id)
        {
            int status = val ? (int)User_Status.normal : (int)User_Status.disabled;
            if (await UsersService.UserStatus(status, id))
                await Table!.QueryAsync();
        }
        private ITableSearchModel usersSearchModel { get; set; } = new UsersSearchModel();
        //全局搜索
        private QueryPivot search = new QueryPivot();
        private async Task<QueryData<UsersData>> OnQueryAsync(QueryPageOptions options)
        {
            var result = new QueryData<UsersData>();
            var searchStatus = await search.GetSearchAsync<UsersSearchModel>(options);
            result.IsAdvanceSearch = searchStatus.IsSearch;
            result.IsSorted = searchStatus.IsOrder;
            var users = await UsersService.GetUsers(search);
            result.Items = users.Items;
            result.TotalCount = users.page_Total;
            return result;
        }
        private async Task<bool> OnSaveAsync(UsersData usersData, ItemChangedType changedType)
        {
            bool result = false;

            string PassWord = string.Empty;

            if (changedType == ItemChangedType.Add)
            {
                var publickey = app.GetSection(EncryptConst.PublicKey).Value ?? "";
                PassWord = ShortId.Create(new ShortIdConfig() { Length = 10 });
                usersData.CipherText = RSA.Encrypt(PassWord, publickey);
                if (string.IsNullOrWhiteSpace(usersData.User_Phone)) usersData.User_Phone ="0"+ ShortId.Create(new ShortIdConfig() { Length=10,OptChars="1234567890"});
                if (string.IsNullOrWhiteSpace(usersData.User_Email)) usersData.User_Email ="0-" +ShortId.Create(new())+"@.com";
            }

            result = await UsersService.SaveUser(usersData);

            if (changedType == ItemChangedType.Add && result)
                await MessageService.Show(new MessageOption()
                {
                    Color = Color.Warning,
                    Content = $"您的密码是:{PassWord},该密码仅展示一次关闭后不再显示,请牢记您的密码.",
                    ShowDismiss = true,
                    IsAutoHide = false,
                    Icon = "fas fa-fw fa-key"
                });

            return result;
        }
        private async Task<bool> OnDeleteAsync(IEnumerable<UsersData> usersData)
        {
            var Ids = usersData.Select(x => x.Id).ToList();
            return await UsersService.DelUser(Ids);
        }
        [NotNull]
        private Modal? UserExcelModel { get; set; }
        private async Task OpenUploadExcel(IEnumerable<UsersData> Items)
        {
            await UserExcelModel.Toggle();
        }
        private Table<UsersData>? Table;
        private async Task<bool> OnExportAsync(ITableExportDataContext<UsersData> context)
        {
            var users = await UsersService.GetUsers(search);
            return await Exporter.ExportAsync(users.Items!, context!.Columns, "用户信息.xlsx");
        }
    }
}
