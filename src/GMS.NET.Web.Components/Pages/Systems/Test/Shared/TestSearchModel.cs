﻿using GMS.NET.Dto.System.Test.Output;
namespace GMS.NET.Web.Components.Pages.Systems.Test.Shared
{
    public class TestSearchModel: ITableSearchModel
    {
        /// <summary>
        /// Desc:测试字段
        /// </summary>
        [QueryPivot(CSharpType.String, Conditional.Like)]
        public string? Test { get; set; }
        public IEnumerable<IFilterAction> GetSearches()
        {
            var ret = new List<IFilterAction>();
            if (!string.IsNullOrWhiteSpace(Test))
                ret.Add(new SearchFilterAction(nameof(TestData.Test), Test));
            return ret;
        }
        public void Reset()
        {
            this.Test = null;
        }
    }
}
