﻿using BootstrapBlazor.Components;
using GMS.NET.Dto.System.Test.Output;
using GMS.NET.Web.Components.Pages.Systems.Custs;
using GMS.NET.Web.Components.Pages.Systems.Custs.Shared;
using GMS.NET.Web.Components.Pages.Systems.Test.Shared;
using GMS.NET.Web.Components.Service.Interface.System;
using GMS.NET.Web.Components.Shared.Global.Misc;
using Microsoft.AspNetCore.Components;
using System.Diagnostics.CodeAnalysis;

namespace GMS.NET.Web.Components.Pages.Systems.Test
{
    public partial class Test
    {
        [Inject]
        [NotNull]
        private ITestService? TestService { get; set; }
        private Table<TestData>? Table;
        private ITableSearchModel TestSearchModel { get; set; } = new TestSearchModel();
        private async Task<QueryData<TestData>> OnQueryAsync(QueryPageOptions options)
        {
            var result = new QueryData<TestData>();
            var query = new QueryPivot();
            var searchStatus = await query.GetSearchAsync<TestSearchModel>(options);
            result.IsAdvanceSearch = searchStatus.IsSearch;
            result.IsSorted = searchStatus.IsOrder;
            var tbresult =await TestService.GetTestData(query);
            result.Items = tbresult.Items;
            result.TotalCount = tbresult.page_Total;
            return result;
        }
        private async Task<bool> OnSaveAsync(TestData testData, ItemChangedType changedType)
        {
            return true;
        }
        private async Task<bool> OnDeleteAsync(IEnumerable<TestData> testData)
        {
            var Ids = testData.Select(x => x.Id).ToList();
            return true;
        }
        private async Task<bool> OnExportAsync(ITableExportDataContext<TestData> context)
        {
            return true;
        }
    }
}
