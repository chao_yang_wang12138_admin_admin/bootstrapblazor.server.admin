﻿using GMS.NET.Dto.System.AccRoles.Enums;
using GMS.NET.Dto.System.AccRoles.Input;
using GMS.NET.Dto.System.AccRoles.Output;
using GMS.NET.Dto.System.AccRoles.Shared;
using GMS.NET.Dto.System.Users.Shared;
using GMS.NET.Web.Components.Pages.Systems.AccRoles.Shared;
using GMS.NET.Web.Components.Service.Interface.System;
using GMS.NET.Web.Components.Shared.ComtsMisc;
using GMS.NET.Web.Components.Shared.Global.Misc;
using Microsoft.AspNetCore.Components;
using System.Diagnostics.CodeAnalysis;

namespace GMS.NET.Web.Components.Pages.Systems.AccRoles
{
    public partial class AccRoles
    {
        [Inject]
        [NotNull]
        private IAccRolesService? accRolesService { get; set; }
        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();
        }
        private async Task<QueryData<RolesData>> OnQueryAsync(QueryPageOptions options)
        {
            var result = new QueryData<RolesData>();
            var search = new QueryPivot();
            var searchStatus = await search.GetSearchAsync<AccRolesSearchModel>(options);
            result.IsAdvanceSearch = searchStatus.IsSearch;
            result.IsSorted = searchStatus.IsOrder;
            var custs = await accRolesService.GetRolesData(search);
            result.Items = custs.Items;
            result.TotalCount = custs.page_Total;
            return result;
        }
        private async Task RolesStatusOnChanged(bool val, Guid Id)
        {
            int status = val ? (int)Role_Status.normal : (int)Role_Status.disabled;
            if (await accRolesService.RoleStatus(status, Id))
                await Table!.QueryAsync();
        }
        private Table<RolesData>? Table;
        private ITableSearchModel custsSearchModel { get; set; } = new AccRolesSearchModel();
        private async Task<bool> OnSaveAsync(RolesData rolesData, ItemChangedType changedType)
        {
            return await accRolesService.SaveRolesData(rolesData);
        }
        private async Task<bool> OnDeleteAsync(IEnumerable<RolesData> rolesData)
        {
            var Ids = rolesData.Select(x => x.Id).ToList();
            return await accRolesService.DelRolesData(Ids);
        }

        //Acc Info
        [NotNull]
        private AccUsers? accUsers { get; set; }
        [NotNull]
        private AccRess? accRess { get; set; }
        //Add Users
        private Table<WaitUsers>? waitUsers { get; set; }
        [NotNull]
        private Modal? UsersModal { get; set; }
        private string? UserModalTitle = "分配用户";
        [NotNull]
        private List<WaitUsers>? SelectedUsers { get; set; } = new();
        private async Task RoleAddUsers(Guid roleId,string roleName)
        {
            UserModalTitle = $"角色{roleName},分配用户";
            accUsers = new();
            accUsers.Role = roleId;
            await waitUsers!.QueryAsync();
            await UsersModal!.Toggle();
        }
        private async Task<QueryData<WaitUsers>> OnQueryUsersAsync(QueryPageOptions options)
        {
            var result = new QueryData<WaitUsers>();
            var search = new QueryPivot()
            {
                IsPage = true,
                PageIndex = 1,
                PageSize = 20,

            };
            if (accUsers is not null)
                search.WhereModel = new()
                {
                    new()
                    {
                        CSharpTypeName=CSharpType.String,
                        ConditionalType=Conditional.NotEqual,
                        FieldName=nameof(UsersData.User_RoleId),
                        FieldValue=accUsers.Role
                    }
                };
            //search.WhereModel = new List<WhereModel>()
            //{
            //    new WhereModel()
            //    {
            //        FieldName="Id",
            //        FieldValue=accUsers.Roles.ToString(),
            //        CSharpTypeName=CSharpType.String,
            //        ConditionalType=Conditional.NotEqual
            //    }
            //};
            var users = await accRolesService.GetWaitUsers(search);
            result.IsAdvanceSearch = true;
            result.IsSorted = true;

            result.Items = users.Items;
            result.TotalCount = users.page_Total;
            return result;
        }
        private async Task RoleAddUsersOnConfirm()
        {
            if (SelectedUsers.Count > 0)
            {
                accUsers.Users = SelectedUsers.Select(x => x.Id).ToList();
                //提交accUsers批量修改
                await accRolesService.SetAccUsers(accUsers);
            }
            await UsersModal!.Toggle();
        }

        //Add Ress
        [NotNull]
        private Modal? RessModal { get; set; }
        private string? RessModalTitle = "分配资源";
        private async Task RoleAddRess(Guid roleId,string roleName)
        {
            RessModalTitle = $"角色{roleName},分配资源";
            accRess = new();
            accRess.Role = roleId;
            var res = await accRolesService.GetWaitRess(roleId);
            var tree = TreeConverter.ConvertToTree(res);
            AsyncItems = new();
            if (tree is not null) tree.ForEach(async x => AsyncItems!.Add(await x.RecursiveCopyAsync()));
            StateHasChanged();
            await RessModal!.Toggle();
        }
        [NotNull]
        private List<TreeViewItem<WaitRess>>? AsyncItems { get; set; }
        private Task OnTreeItemChecked(List<TreeViewItem<WaitRess>> node)
        {
            accRess.Ress = node.Select(x => x.Value.Id).ToList();
            return Task.CompletedTask;
        }
        private async Task RoleAddRessOnConfirm()
        {
            await accRolesService.SetAccRess(accRess);
            await RessModal!.Toggle();
        }
    }
}
