﻿using GMS.NET.Dto.System.AccRoles.Shared;

namespace GMS.NET.Web.Components.Pages.Systems.AccRoles.Shared
{
    public class AccRolesSearchModel : ITableSearchModel
    {
        [QueryPivot(CSharpType.String, Conditional.Like)]
        public string? Role_Name { get; set; }

        public IEnumerable<IFilterAction> GetSearches()
        {
            var ret = new List<IFilterAction>();
            if (!string.IsNullOrWhiteSpace(Role_Name))
                ret.Add(new SearchFilterAction(nameof(RolesData.Role_Name), Role_Name));
            return ret;
        }

        public void Reset()
        {
            this.Role_Name = null;
        }

    }
}
