﻿using BootstrapBlazor.Components;
using GMS.NET.Web.Components.Shared.Global.Misc;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using System.Diagnostics.CodeAnalysis;

namespace GMS.NET.Web.Components.Pages.Systems.Tools
{
    public partial class Tools
    {
        [Inject]
        [NotNull]
        private IConfiguration? Configuration { get; set; }
        [Inject]
        [NotNull]
        private MessageService? Message { get; set; }
        private bool IsValidates { get; set; }
        protected override Task OnInitializedAsync()
        {
            IsValidates = false;
            return base.OnInitializedAsync();
        }
        /// <summary>
        /// 口令校验
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        private Task Authentication(string password)
        {
            var orgPassword = Configuration.GetSection(AppSetting.ServerTools).Value;
            if (orgPassword == password)
            {
                IsValidates = true;
                StateHasChanged();
            }
            else
            {
                Message.Show(new MessageOption()
                {
                    Color = Color.Danger,
                    Icon = "fas fa-circle-xmark",
                    Content = "口令错误"
                });
            }
            return Task.CompletedTask;
        }
    }
}
