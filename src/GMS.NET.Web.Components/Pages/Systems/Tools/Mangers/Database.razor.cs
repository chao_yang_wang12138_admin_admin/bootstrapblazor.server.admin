﻿using BootstrapBlazor.Components;
using GMS.NET.Web.Components.Service.Interface.System;
using Microsoft.AspNetCore.Components;
using System.Diagnostics.CodeAnalysis;

namespace GMS.NET.Web.Components.Pages.Systems.Tools.Mangers
{
    public partial class Database
    {
        [Inject]
        [NotNull]
        private IToolsService? ToolsService { get; set; }
        [Inject]
        [NotNull]
        private DownloadService? DownloadService { get; set; }
        [Inject]
        [NotNull]
        private MessageService? Message { get; set; }
        protected override Task OnInitializedAsync()
        {
            DatabaseRadioDatas = new List<SelectedItem>(2)
            {
                new SelectedItem("1", "实体建表模式"){ Active=true},
                new SelectedItem("2", "Excel建表和实体模式")
            };
            return base.OnInitializedAsync();
        }
        [NotNull]
        private IEnumerable<SelectedItem>? DatabaseRadioDatas { get; set; }
        private string DatabaseCreateType = "1";
        private Task DatabaseRadioSelectedChanged(IEnumerable<SelectedItem> values, string val)
        {
            DatabaseCreateType = val;
            StateHasChanged();
            return Task.CompletedTask;
        }
        private string DatabaseNameSpace = "GMS.NET.Core.Entitys.System";
        private async Task DatabaseCreateTask1()
        {
            if (!string.IsNullOrWhiteSpace(DatabaseNameSpace))
                await ToolsService.EntitysToDatabaseTable(DatabaseNameSpace);
            else
                await Message.Show(new MessageOption() { Color = Color.Warning, Content = "请输入命名空间" });
        }
        private async Task DatabaseCreateTask2DownTemp()
        {
            var Stream = await ToolsService.DatabaseExcelTempStream();
            if (Stream is not null) await DownloadService.DownloadFromStreamAsync("数据库创建模板.xlsx", Stream);
        }
        private async Task DatabaseCreateTask2(UploadFile file)
        {
            using var stream = file.File!.OpenReadStream();
            await ToolsService.CreateExcelToEntitysFileAndDbTable(stream, DatabaseNameSpace);
        }
    }
}
