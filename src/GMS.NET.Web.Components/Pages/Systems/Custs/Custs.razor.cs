﻿using GMS.NET.Dto.System.Custs.Enums;
using GMS.NET.Dto.System.Custs.Shared;
using GMS.NET.Dto.System.Users.Enums;
using GMS.NET.Web.Components.Pages.Systems.Custs.Shared;
using GMS.NET.Web.Components.Service.Interface.System;
using GMS.NET.Web.Components.Shared.Global.Misc;
using Microsoft.AspNetCore.Components;
using System.Diagnostics.CodeAnalysis;

namespace GMS.NET.Web.Components.Pages.Systems.Custs
{
    public partial class Custs
    {
        [Inject]
        [NotNull]
        private ICustsService? CustsService { get; set; }
        private bool IsShowNotes { get; set; } = true;
        protected override async Task OnInitializedAsync()
        {
            IsShowNotes = await CustsService.CheckUsersConfirmOperCustsNotes(false);
        }
        private async Task OnStateChanged(CheckboxState state, string value)
        {
            IsShowNotes = await CustsService.CheckUsersConfirmOperCustsNotes(state == CheckboxState.Checked);
            StateHasChanged();
        }
        private async Task<QueryData<CustsData>> OnQueryAsync(QueryPageOptions options)
        {
            var result = new QueryData<CustsData>();
            var search = new QueryPivot();
            var searchStatus = await search.GetSearchAsync<CustsSearchModel>(options);
            result.IsAdvanceSearch = searchStatus.IsSearch;
            result.IsSorted = searchStatus.IsOrder;
            var custs = await CustsService.GetCustsData(search);
            result.Items = custs.Items;
            result.TotalCount = custs.page_Total;
            return result;
        }
        private async Task CustsStatusOnChanged(bool val, Guid id)
        {
            int status = val ? (int)Cust_Status.normal : (int)Cust_Status.disabled;
            if (await CustsService.CustsStatus(status, id))
                await Table!.QueryAsync();
        }
        private Table<CustsData>? Table;
        private ITableSearchModel custsSearchModel { get; set; } = new CustsSearchModel();
        private async Task<bool> OnSaveAsync(CustsData custsData, ItemChangedType changedType)
        {
            return await CustsService.SaveCustsData(custsData);
        }
        private async Task RecoverCusts(IEnumerable<CustsData> custsDatas)
        {
            var Ids = custsDatas.Select(x => x.Id).ToList();
            await CustsService.RecoverCustsData(Ids);
        }
        private async Task<bool> OnDeleteAsync(IEnumerable<CustsData> custsDatas)
        {
            var Ids = custsDatas.Select(x => x.Id).ToList();
            return await CustsService.DelCustsData(Ids);
        }
    }
}
