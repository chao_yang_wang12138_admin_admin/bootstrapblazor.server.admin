﻿using GMS.NET.Dto.System.Custs.Shared;

namespace GMS.NET.Web.Components.Pages.Systems.Custs.Shared
{
    public class CustsSearchModel : ITableSearchModel
    {
        [QueryPivot(CSharpType.String,Conditional.Like)]
        public string? Cust_Name { get; set; }

        public IEnumerable<IFilterAction> GetSearches()
        {
            var ret = new List<IFilterAction>();
            if (!string.IsNullOrWhiteSpace(Cust_Name))
                ret.Add(new SearchFilterAction(nameof(CustsData.Cust_Name), Cust_Name));
            return ret;
        }

        public void Reset()
        {
            this.Cust_Name = null;
        }
    }
}
