﻿using GMS.NET.Dto.System.Menus.Enums;
using GMS.NET.Dto.System.Menus.Shared;
using GMS.NET.Dto.System.Users.Shared;
using GMS.NET.Web.Components.Service.Interface.System;
using Microsoft.AspNetCore.Components;
using System.Diagnostics.CodeAnalysis;

namespace GMS.NET.Web.Components.Pages.Systems.Menus
{
    public partial class Menus
    {
        [NotNull]
        private List<MenusTree>? TreeItems { get; set; }
        [Inject]
        [NotNull]
        private IMenusService? MenusService { get; set; }
        private Table<MenusTree>? Table;
        /// <summary>
        /// 快捷切换状态
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private async Task ResDisabledOnChanged(bool val, Guid id)
        {
            int res_Disabled = val ? (int)Res_Disabled.normal : (int)Res_Disabled.disabled;
            if (await MenusService.MenusTreeStatus(res_Disabled, id))
                await Table!.QueryAsync();
        }
        /// <summary>
        /// OnInitialized 方法
        /// </summary>
        protected override void OnInitialized()
        {
            base.OnInitialized();
        }
        private async Task<QueryData<MenusTree>> OnQueryAsync(QueryPageOptions options)
        {
            var result = new QueryData<MenusTree>();
            TreeItems = await MenusService.GetMenusTree(new Guid(DataConst.TopParentGuid));
            result.Items = TreeItems;
            return result;
        }
        private static Task<IEnumerable<TableTreeNode<MenusTree>>> TreeNodeConverter(IEnumerable<MenusTree> items)
        {
            // 构造树状数据结构
            var ret = BuildTreeNodes(items, new Guid(DataConst.TopParentGuid));
            return Task.FromResult(ret);

            IEnumerable<TableTreeNode<MenusTree>> BuildTreeNodes(IEnumerable<MenusTree> items, Guid parentId)
            {
                var ret = new List<TableTreeNode<MenusTree>>();
                ret.AddRange(items.Where(i => i.Res_ParentId == parentId).Select((tree, index) => new TableTreeNode<MenusTree>(tree)
                {
                    // 是否存在子项
                    HasChildren = true,
                    // 是否默认展开
                    IsExpand = false,
                    // 获得子项集合
                    Items = BuildTreeNodes(items.Where(i => i.Res_ParentId == tree.Id), tree.Id)
                }));
                return ret;
            }
        }
        private async Task<IEnumerable<TableTreeNode<MenusTree>>> OnTreeExpand(MenusTree tree)
        {
            var childrens = await MenusService.GetMenusTree(tree.Id);
            return childrens.Select(i => new TableTreeNode<MenusTree>(i) { HasChildren = true, IsExpand = false });
        }
        private async Task<bool> OnSaveAsync(MenusTree menusTree, ItemChangedType changedType)
        {
            if (menusTree.Res_Icon is not null && menusTree.Res_Icon.Split(" ").Length == 2)
                menusTree.Res_Icon = $"{menusTree.Res_Icon!.Split(" ")[0]} fa-fw {menusTree.Res_Icon.Split(" ")[1]}";
            return await MenusService.SaveMenusTree(menusTree);
        }
        private async Task<bool> OnDeleteAsync(IEnumerable<MenusTree> menusTree)
        {
            var menusTreeIdList = menusTree.Select(x => x.Id).ToList();
            return await MenusService.DelMenusTree(menusTreeIdList);
        }
    }
}
