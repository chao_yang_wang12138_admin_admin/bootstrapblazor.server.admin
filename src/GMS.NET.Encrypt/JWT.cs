﻿using System.IdentityModel.Tokens.Jwt;

namespace GMS.NET.Encrypt
{
    public class JWT
    {
        public static string GetJWTTypeValue(string AccessToken, string Type)
        {
            if (string.IsNullOrWhiteSpace(AccessToken)) return "AccessToken Is Empty";
            var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = jwtSecurityTokenHandler.ReadJwtToken(AccessToken);
            return jwtSecurityToken.Claims.FirstOrDefault(c => c.Type == Type)!.Value;
        }
    }
}