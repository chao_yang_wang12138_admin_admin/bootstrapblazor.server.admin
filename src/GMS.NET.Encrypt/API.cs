﻿namespace GMS.NET.Encrypt
{
    /// <summary>
    /// api加签
    /// </summary>
    public class API
    {
        /// <summary>
        /// 接口签名
        /// </summary>
        /// <param name="Data"></param>
        /// <param name="Timestamp"></param>
        /// <param name="ServerSalt"></param>
        /// <returns></returns>
        public static string GetSignature(string Timestamp, string ServerKey)
        {
            // 设置序列化选项
            var CheckInfo = $"{Timestamp}&{ServerKey}";
            var CheckInfoOrder = CheckInfo.OrderBy(x => x);
            var SHA512CheckInfo = $"{string.Join("", CheckInfoOrder)}";
            return SHA.Hash512(SHA512CheckInfo);
        }
    }
}
