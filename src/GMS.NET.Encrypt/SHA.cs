﻿using System.Security.Cryptography;
using System.Text;

namespace GMS.NET.Encrypt
{
    /// <summary>
    /// SHA哈希
    /// </summary>
    public class SHA
    {
        /// <summary>
        /// 64位十六进制 加密
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string Hash256(string input)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);
                byte[] hashBytes = sha256.ComputeHash(inputBytes);

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    builder.Append(hashBytes[i].ToString("X2"));
                }

                return builder.ToString();
            }
        }
        /// <summary>
        /// 128位十六进制 加密
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string Hash512(string input)
        {
            using (SHA512 sha512 = SHA512.Create())
            {
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);
                byte[] hashBytes = sha512.ComputeHash(inputBytes);

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    builder.Append(hashBytes[i].ToString("X2"));
                }

                return builder.ToString();
            }
        }
    }
}
