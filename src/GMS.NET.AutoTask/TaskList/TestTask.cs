﻿namespace GMS.NET.AutoTask.TaskList
{
    [JobDetail("testtask","测试任务"),
    Cron(TaskCronConst.Every3s, CronStringFormat.WithSeconds, RunOnStart = true)]
    public class TestTask : IJob
    {
        /// <summary>
        /// 消息队列
        /// 任务非常多时可以使用保证任务执行的稳定性
        /// </summary>
        private readonly ITaskQueue _taskQueue;
        public TestTask(ITaskQueue taskQueue)
        {
            _taskQueue = taskQueue;
        }

        /// <summary>
        /// 任务执行方法
        /// </summary>
        /// <param name="context"></param>
        /// <param name="stoppingToken"></param>
        /// <returns></returns>
        public async Task ExecuteAsync(JobExecutingContext context, CancellationToken stoppingToken)
        {
            ///异步队列
            await _taskQueue.EnqueueAsync(async (provider, token) =>
            {
                Console.WriteLine($"{DateTime.Now} A test task was performed.");
                await ValueTask.CompletedTask;
            });
        }
    }
}
