IHost host = Host.CreateDefaultBuilder(args)
    .UseWindowsService()
    .Inject()
    .ConfigureServices(services =>
    {
        services.AddTaskQueue();
        services.AddSchedule(options =>
        {
            options.AddJob(App.EffectiveTypes.ScanToBuilders());
        });
    })
    .Build();

await host.RunAsync();
